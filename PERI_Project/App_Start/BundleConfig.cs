﻿using System.Web;
using System.Web.Optimization;

namespace PERI_Project
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //Untuk Script js template User Login
            bundles.Add(new ScriptBundle("~/Scripts_UserLogin/js").Include(
                      "~/Scripts_UserLogin/jquery-3.2.1.min.js",
                      "~/ Scripts_UserLogin / animsition.min.js",
                      "~/Scripts_UserLogin/popper.js",
                      "~/Scripts_UserLogin/bootstrap.min.js",
                      "~/Scripts_UserLogin/select2.min.js",
                      "~/Scripts_UserLogin/moment.min.js",
                      "~/Scripts_UserLogin/daterangepicker.js",
                      "~/Scripts_UserLogin/countdowntime.js",
                      "~/Scripts_UserLogin/main.js"
                      ));

            //Untuk Content css style template User Login
            bundles.Add(new StyleBundle("~/Content_UserLogin/css").Include(
                      "~/Content_UserLogin//bootstrap.min.css",
                      "~/Content_UserLogin/bootstrap.min.css",
                      "~/Content_UserLogin/font-awesome.min.css",
                      "~/Content_UserLogin/material-design-iconic-font.min.css",
                      "~/Content_UserLogin/animate.css",
                      "~/Content_UserLogin/hamburgers.min.css",
                      "~/Content_UserLogin/animsition.min.css",
                      "~/Content_UserLogin/select2.min.css",
                      "~/Content_UserLogin/daterangepicker.css",
                      "~/Content_UserLogin/util.css",
                      "~/Content_UserLogin/main.css"
                ));

            //Untuk Scripts js template Admin Login
            bundles.Add(new ScriptBundle("~/Scripts_AdminLogin/js").Include(
                     "~/Scripts_AdminLogin/jquery.min.js",
                     "~/Scripts_AdminLogin/bootstrap.bundle.min.js",
                     "~/Scripts_AdminLogin/jquery.easing.min.js",
                     "~/Scripts_AdminLogin/sb-admin-2.min.js",
                      "~/Scripts_AdminLogin/jquery.validate.min.js",
                     "~/Scripts_AdminLogin/jquery.validate.unobtrusive.min.js"
                ));

            //Untuk Content css style templae HomeCustomer
            bundles.Add(new StyleBundle("~/Content_HomeCustomer/css").Include(
                      "~/Content_HomeCustomer/open-iconic-bootstrap.min.css",
                      "~/Content_HomeCustomer/animate.css",
                      "~/Content_HomeCustomer/owl.carousel.min.css",
                      "~/Content_HomeCustomer/owl.theme.default.min.css",
                      "~/Content_HomeCustomer/magnific-popup.css",
                      "~/Content_HomeCustomer/aos.css",
                      "~/Content_HomeCustomer/ionicons.min.css",
                      "~/Content_HomeCustomer/bootstrap-datepicker.css",
                      "~/Content_HomeCustomer/jquery.timepicker.css",
                      "~/Content_HomeCustomer/flaticon.css",
                      "~/Content_HomeCustomer/icomoon.css",
                      "~/Content_HomeCustomer/style.css"
                ));
        }
    }
}
