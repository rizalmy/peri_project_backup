﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PERI_Project.Models
{
    public class spGetIncomeAnnual2_Result
    {
        public string Month { get; set; }
        public Nullable<double> Income { get; set; }
    }
}