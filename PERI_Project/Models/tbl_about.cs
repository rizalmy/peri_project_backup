﻿

namespace PERI_Project.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    public partial class tbl_about
    {
        public tbl_about()
        {
            Profile_Image = "~/Content/images/about/about_default.jpg";
        }
        public int AboutID { get; set; }
        public string Profile_Image { get; set; }
        public string Main_Title { get; set; }
        public string Description { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
    }
}