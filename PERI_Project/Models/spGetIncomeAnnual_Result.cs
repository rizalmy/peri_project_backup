﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PERI_Project.Models
{
    public partial class spGetIncomeAnnual_Result
    {
        public Nullable<int> Month { get; set; }
        public Nullable<double> Income { get; set; }
    }
}