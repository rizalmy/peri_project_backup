﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PERI_Project.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web;
    public class tbl_UploadDebt
    {
        public tbl_UploadDebt()
        {
            ImagePath = "~/Content/images/debt_proof/invoice_default.png";
        }
        public int pk_id_UploadDebt { get; set; }
        public Nullable<int> Debt_id { get; set; }
        public string ImagePath { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
        public string BankName { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccountNumber { get; set; }
    }
}