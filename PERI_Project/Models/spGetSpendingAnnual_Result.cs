﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PERI_Project.Models
{
    public class spGetSpendingAnnual_Result
    {
        public Nullable<int> Month { get; set; }
        public Nullable<double> spending { get; set; }
    }
}