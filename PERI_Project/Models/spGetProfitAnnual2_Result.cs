﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PERI_Project.Models
{
    public class spGetProfitAnnual2_Result
    {
        public string Month { get; set; }
        public Nullable<double> Profit { get; set; }
    }
}