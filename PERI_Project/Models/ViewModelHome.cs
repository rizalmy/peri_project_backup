﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PERI_Project.Models
{
    public class ViewModelHome
    {
        public List<tbl_banner> banner { get; set; }
        public List<tbl_product> product { get; set; }

        public List<spGetProfitAnnual2_Result> annualProfit2 { get; set; }
        public List<spGetIncomeAnnual2_Result> annualIncome2 { get; set; }
        public List<spGetSpendingAnnual2_Result> annualSpending2 { get; set; }
    }
}