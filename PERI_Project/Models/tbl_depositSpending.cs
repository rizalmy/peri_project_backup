//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PERI_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_depositSpending
    {
        public int pk_id_depositSpending { get; set; }
        public Nullable<int> fk_id_spending { get; set; }
        public Nullable<double> deposit { get; set; }
        public Nullable<System.DateTime> date { get; set; }
    
        public virtual tbl_spending tbl_spending { get; set; }
    }
}
