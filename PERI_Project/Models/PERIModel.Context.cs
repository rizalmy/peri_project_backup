﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PERI_Project.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class PERIEntityDB : DbContext
    {
        public PERIEntityDB()
            : base("name=PERIEntityDB")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tbl_Admin> tbl_Admin { get; set; }
        public virtual DbSet<tbl_Customer> tbl_Customer { get; set; }
        public virtual DbSet<tbl_customerDebt> tbl_customerDebt { get; set; }
        public virtual DbSet<tbl_customerOrder> tbl_customerOrder { get; set; }
        public virtual DbSet<tbl_design> tbl_design { get; set; }
        public virtual DbSet<tbl_Addition> tbl_Addition { get; set; }
        public virtual DbSet<tbl_order> tbl_order { get; set; }
        public virtual DbSet<tbl_otherIncome> tbl_otherIncome { get; set; }
        public virtual DbSet<tbl_payment> tbl_payment { get; set; }
        public virtual DbSet<tbl_product> tbl_product { get; set; }
        public virtual DbSet<tbl_RoleUser> tbl_RoleUser { get; set; }
        public virtual DbSet<tbl_cart> tbl_cart { get; set; }
        public virtual DbSet<tbl_banner> tbl_banner { get; set; }
        public virtual DbSet<tbl_spendingItem> tbl_spendingItem { get; set; }
        public virtual DbSet<tbl_spending> tbl_spending { get; set; }
        public virtual DbSet<tbl_Owner_debt> tbl_Owner_debt { get; set; }
        public virtual DbSet<tbl_contact> tbl_contact { get; set; }
        public virtual DbSet<tbl_OrderRequest> tbl_OrderRequest { get; set; }
        public virtual DbSet<tbl_HistoryCustomerDebt> tbl_HistoryCustomerDebt { get; set; }
        public virtual DbSet<tbl_HistoryOwnerDebt> tbl_HistoryOwnerDebt { get; set; }
        public virtual DbSet<tbl_depositPayment> tbl_depositPayment { get; set; }
        public virtual DbSet<tbl_depositSpending> tbl_depositSpending { get; set; }
        public virtual DbSet<tbl_about> tbl_about { get; set; }
        public virtual DbSet<tbl_otherSpending> tbl_otherSpending { get; set; }
        public virtual DbSet<tbl_UploadPayment> tbl_UploadPayment { get; set; }
        public virtual DbSet<tbl_UploadDebt> tbl_UploadDebt { get; set; }
        public virtual ObjectResult<Nullable<double>> spGetIncomeTotal(Nullable<int> paramYear)
        {
            var paramYearParameter = paramYear.HasValue ?
                new ObjectParameter("paramYear", paramYear) :
                new ObjectParameter("paramYear", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<double>>("spGetIncomeTotal", paramYearParameter);
        }
    
        public virtual ObjectResult<Nullable<double>> spGetProfitTotal(Nullable<int> paramYear)
        {
            var paramYearParameter = paramYear.HasValue ?
                new ObjectParameter("paramYear", paramYear) :
                new ObjectParameter("paramYear", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<double>>("spGetProfitTotal", paramYearParameter);
        }
    
        public virtual ObjectResult<Nullable<double>> spGetSpendingTotal(Nullable<int> paramYear)
        {
            var paramYearParameter = paramYear.HasValue ?
                new ObjectParameter("paramYear", paramYear) :
                new ObjectParameter("paramYear", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<double>>("spGetSpendingTotal", paramYearParameter);
        }

        public virtual ObjectResult<spGetProfitAnnual_Result> spGetProfitAnnual(Nullable<int> paramYear)
        {
            var paramYearParameter = paramYear.HasValue ?
                new ObjectParameter("paramYear", paramYear) :
                new ObjectParameter("paramYear", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetProfitAnnual_Result>("spGetProfitAnnual", paramYearParameter);
        }

        public virtual ObjectResult<spGetIncomeAnnual_Result> spGetIncomeAnnual(Nullable<int> paramYear)
        {
            var paramYearParameter = paramYear.HasValue ?
                new ObjectParameter("paramYear", paramYear) :
                new ObjectParameter("paramYear", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetIncomeAnnual_Result>("spGetIncomeAnnual", paramYearParameter);
        }

        public virtual ObjectResult<spGetSpendingAnnual_Result> spGetSpendingAnnual(Nullable<int> paramYear)
        {
            var paramYearParameter = paramYear.HasValue ?
                new ObjectParameter("paramYear", paramYear) :
                new ObjectParameter("paramYear", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetSpendingAnnual_Result>("spGetSpendingAnnual", paramYearParameter);
        }

        public virtual ObjectResult<spGetProfitAnnual2_Result> spGetProfitAnnual2(Nullable<int> paramYear)
        {
            var paramYearParameter = paramYear.HasValue ?
                new ObjectParameter("paramYear", paramYear) :
                new ObjectParameter("paramYear", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetProfitAnnual2_Result>("spGetProfitAnnual2", paramYearParameter);
        }

        public virtual ObjectResult<spGetIncomeAnnual2_Result> spGetIncomeAnnual2(Nullable<int> paramYear)
        {
            var paramYearParameter = paramYear.HasValue ?
                new ObjectParameter("paramYear", paramYear) :
                new ObjectParameter("paramYear", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetIncomeAnnual2_Result>("spGetIncomeAnnual2", paramYearParameter);
        }

        public virtual ObjectResult<spGetSpendingAnnual2_Result> spGetSpendingAnnual2(Nullable<int> paramYear)
        {
            var paramYearParameter = paramYear.HasValue ?
                new ObjectParameter("paramYear", paramYear) :
                new ObjectParameter("paramYear", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetSpendingAnnual2_Result>("spGetSpendingAnnual2", paramYearParameter);
        }
    }
}
