﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;
using System.IO;

namespace PERI_Project.Controllers
{
    public class BannerController : Controller
    {
        PERIEntityDB db = new PERIEntityDB();
        // GET: Banner
        public ActionResult Index()
        {
            return View(db.tbl_banner.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_banner banner = db.tbl_banner.Find(id);
            if (banner == null)
            {
                return HttpNotFound();
            }
            return View(banner);
        }

        // GET: product/Create
        public ActionResult Create(int id = 0)
        {
            tbl_banner banner = new tbl_banner();
            return View(banner);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(/*[Bind(Include = "pk_id_product,product_name,product_price")]*/ tbl_banner banner)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string filename = Path.GetFileNameWithoutExtension(banner.ImageFile.FileName);
                    string extension = Path.GetExtension(banner.ImageFile.FileName);
                    filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                    banner.Banner_Image = "~/Content/images/banner/" + filename;
                    filename = Path.Combine(Server.MapPath("~/Content/images/banner/"), filename);

                    if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
                    {
                        if (banner.ImageFile.ContentLength <= 500000)
                        {
                            db.tbl_banner.Add(banner);

                            if (db.SaveChanges() > 0)
                            {
                                banner.ImageFile.SaveAs(filename);
                                TempData["msg"] = "Success data banner added";
                                ModelState.Clear();
                                return RedirectToAction("Index", "Banner");
                            }

                        }
                        else
                        {
                            ViewBag.msg = "File Size must be Equal or less than 500kb";
                        }
                    }
                    else
                    {
                        ViewBag.msg = "Invalid File Type ! Select file type [ jpg or png ]";
                    }
                }
                catch (Exception error)
                {
                    throw error;
                }
            }
            return View(banner);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var banner = db.tbl_banner.Find(id);
            Session["imgPath"] = banner.Banner_Image;
            if (banner == null)
            {
                return HttpNotFound();
            }
            return View(banner);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_banner banner)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (banner.ImageFile != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(banner.ImageFile.FileName);
                        string extension = Path.GetExtension(banner.ImageFile.FileName);
                        filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                        banner.Banner_Image = "~/Content/images/banner/" + filename;
                        filename = Path.Combine(Server.MapPath("~/Content/images/banner/"), filename);

                        if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
                        {
                            if (banner.ImageFile.ContentLength <= 500000)
                            {
                                db.Entry(banner).State = EntityState.Modified;
                                string oldImgPath = Request.MapPath(Session["imgPath"].ToString());

                                if (db.SaveChanges() > 0)
                                {
                                    banner.ImageFile.SaveAs(filename);

                                    if (System.IO.File.Exists(oldImgPath))
                                    {
                                        System.IO.File.Delete(oldImgPath);
                                    }
                                    TempData["msg"] = "Successful banner data updated";
                                    return RedirectToAction("Index", "Banner");
                                }

                            }
                            else
                            {
                                ViewBag.msg = "File Size must be Equal or less than 500kb";
                            }
                        }
                        else
                        {
                            ViewBag.msg = "Invalid File Type ! Select file type [ jpg or png ]";
                        }
                    }
                    else
                    {
                        banner.Banner_Image = Session["imgPath"].ToString();
                        db.Entry(banner).State = EntityState.Modified;
                        if (db.SaveChanges() > 0)
                        {
                            TempData["msg"] = "Successful banner data updated";
                            return RedirectToAction("Index", "Banner");
                        }
                    }
                }
                catch (Exception error)
                {
                    throw error;
                }
            }
            return View();
        }
    }
}