﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Admin
{
    public class paymentController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: payment
        public ActionResult Index()
        {
            var tbl_payment = db.tbl_payment.Include(t => t.tbl_customerOrder).Where(item => item.order_status == "Not Yet" || item.order_status == "Debt").OrderByDescending(item => item.pk_id_payment);
            return View(tbl_payment.ToList());
        }

        public ActionResult IndexOnProgress()
        {
            var tbl_payment = db.tbl_payment.Include(t => t.tbl_customerOrder).Where(item => item.order_status == "On Progress" || item.order_status == "Deposit").OrderByDescending(item => item.pk_id_payment);
            return View(tbl_payment.ToList());
        }
        public ActionResult IndexDone()
        {
            var tbl_payment = db.tbl_payment.Include(t => t.tbl_customerOrder).Where(item => item.order_status == "Done" || item.order_status == "Deposit").OrderByDescending(item => item.pk_id_payment);
            return View(tbl_payment.ToList());
        }

        // GET: payment = payment status
        public ActionResult IndexPaymentNotYet()
        {
            var tbl_payment = db.tbl_payment.Include(t => t.tbl_customerOrder).Where(item => item.payment_status == "Not Yet").OrderByDescending(item => item.pk_id_payment);
            return View(tbl_payment.ToList());
        }
        public ActionResult IndexPaymentDebt()
        {
            var tbl_payment = db.tbl_payment.Include(t => t.tbl_customerOrder).Where(item => item.payment_status == "Debt").OrderByDescending(item => item.pk_id_payment);
            return View(tbl_payment.ToList());
        }
        public ActionResult IndexPaymentDeposit()
        {
            var tbl_payment = db.tbl_payment.Include(t => t.tbl_customerOrder).Where(item => item.payment_status == "Deposit").OrderByDescending(item => item.pk_id_payment);
            return View(tbl_payment.ToList());
        }
        public ActionResult IndexPaymentDone()
        {
            var tbl_payment = db.tbl_payment.Include(t => t.tbl_customerOrder).Where(item => item.payment_status == "Done").OrderByDescending(item => item.pk_id_payment);
            return View(tbl_payment.ToList());
        }




        public ActionResult Income()
        {
            var tbl_payment = db.tbl_payment.Include(t => t.tbl_customerOrder).OrderByDescending(item => item.pk_id_payment);
            return View(tbl_payment.ToList());
        }

        // GET: payment/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            tbl_payment tbl_payment = db.tbl_payment.Find(id);
            Session["paymentStatus"] = tbl_payment.payment_status;
            ViewBag.NoPayment = tbl_payment.pk_id_payment;
            ViewBag.Name = tbl_payment.tbl_customerOrder.name;
            ViewBag.TotalPrice = tbl_payment.total_price;
            ViewBag.Payment = tbl_payment.payment;
            ViewBag.BankName = tbl_payment.BankName;
            ViewBag.BankAccountName = tbl_payment.BankAccountName;
            ViewBag.BankAccountNumber = tbl_payment.BankAccountNumber;
            Session["payment"] = tbl_payment.ImagePath;
            var order = db.tbl_order.Where(item => item.fk_customer_id == tbl_payment.fk_id_customerOrder);
            if (tbl_payment == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: payment/Create
        public ActionResult Create()
        {
            ViewBag.fk_id_customerOrder = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name");
            return View();
        }

        // POST: payment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_payment,fk_id_customerOrder,total_price,payment,payment_status,order_status,date")] tbl_payment tbl_payment)
        {

            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.tbl_payment.Add(tbl_payment);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message;
                    }
                }

            }

            ViewBag.fk_id_customerOrder = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", tbl_payment.fk_id_customerOrder);
            return View(tbl_payment);
        }

        // GET: payment/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_payment tbl_payment = db.tbl_payment.Find(id);
            TempData["customername"] = tbl_payment.tbl_customerOrder.name;
            if (tbl_payment == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_customerOrder = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", tbl_payment.fk_id_customerOrder);
            return View(tbl_payment);
        }

        // POST: payment/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit(tbl_payment order_total)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Entry(order_total).State = EntityState.Modified;
                        order_total.date = DateTime.Now;
                        tbl_customerDebt debt2 = new tbl_customerDebt();
                        if (order_total.payment > order_total.total_price)
                        {
                            tbl_depositPayment deposit = new tbl_depositPayment();
                            deposit.fk_id_payment = order_total.pk_id_payment;
                            deposit.deposit = order_total.payment - order_total.total_price;
                            //deposit.date = DateTime.Now;
                            deposit.date = order_total.date;
                            db.tbl_depositPayment.Add(deposit);
                            //order_total.payment = order_total.total_price;
                            order_total.payment_status = "Deposit";
                        }
                        else if (order_total.payment == order_total.total_price)
                        {
                            order_total.payment_status = "Done";
                        }
                        else
                        {
                            order_total.payment_status = "Debt";
                            
                            debt2.fk_id_payment = order_total.pk_id_payment;
                            debt2.Debt = order_total.total_price - order_total.payment;
                            debt2.debt_status = "Not Yet";
                            debt2.date = DateTime.Now;
                            db.tbl_customerDebt.Add(debt2);
                            db.SaveChanges();
                        }

                        order_total.order_status = "On Progress";
                        db.Entry(order_total).State = EntityState.Modified;
                        db.SaveChanges();

                        
                        //send EMail
                        var email = db.tbl_customerOrder.Where(item => item.pk_id_customerOrder == order_total.fk_id_customerOrder).FirstOrDefault();
                        var order = db.tbl_order.Where(item => item.fk_customer_id == email.pk_id_customerOrder);
                        var link = "<a href='" + Request.Url.Scheme + "://" + Request.Url.Authority + @Url.Action("getDebtID", "HomeCustomer", new { id = debt2.pk_id_cDebt }) + "'>Click here to Pay</a>";
                        string data =
                            "Dear " + email.name + "<br/>" +
                            "Your Payment has been accepted <br/>" +
                            "No. Payment : " + order_total.pk_id_payment + "<br/>" +
                            "Total Price : " + order_total.total_price + "<br/>" +
                            "Your Payment : " + order_total.payment + "<br/>";
                        if (debt2.pk_id_cDebt != 0)
                        {
                            data += 
                                "Your Payment isn't enough, Here's your debt detail<br/>" +
                                "Debt ID : "+debt2.pk_id_cDebt+ " ("+link+")<br/>" +
                                "Debt : "+debt2.Debt+ "<br/>" +
                                "You can pay by uploading payment proof on Debt payment with inputting the Debt ID <br/>";  
                        }
                        data+=    "Start Date : " + order_total.date + "<br/>" +
                            "Estimate when the Order is done : " + order_total.estimasi + "<br/>" +
                            "===========================================<br/>" +
                            "Your Product : <br/>" +
                            "-----------------------------------------------<br/>";
                        foreach (var item in order)
                        {
                            data +=
                                "Product Name : " + item.tbl_product.product_name + "<br/>" +
                                "Width : " + item.width_meter + " Lenght : " + item.lenght_meter + "<br/>" +
                                "Amount : " + item.amount + "<br/>" +
                                "Details : " + item.details_order + "<br/>" +
                                "-----------------------------------------<br/>";
                        }

                        data +=
                            "Please wait for your order is finished, Thank you!<br/>" +
                            "===============================================================<br/>";
                        var fromEmail = new MailAddress("periofficial.company@gmail.com", "PERI");
                        var toEmail = new MailAddress(email.email);
                        var fromEmailPassword = "logPeri987"; // Replace with actual password


                        string subject = "";
                        string body = "";
                        subject = "Your Payment is Accepted!";
                        body = data;



                        var smtp = new SmtpClient
                        {
                            Host = "smtp.gmail.com",
                            Port = 587,
                            EnableSsl = true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = true,
                            Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
                        };

                        using (var msg = new MailMessage(fromEmail, toEmail)
                        {
                            Subject = subject,
                            Body = body,
                            IsBodyHtml = true

                        })
                            smtp.Send(msg);
                        //send Email

                        transaction.Commit();
                        return RedirectToAction("IndexOnProgress", "payment");
                        //if (order_total.payment_status == "Done")
                        //{
                        //    return RedirectToAction("IndexPaymentDone");
                        //}
                        //else if (order_total.payment_status == "Debt")
                        //{
                        //    return RedirectToAction("IndexPaymentDebt");
                        //}
                        //else if (order_total.payment_status == "Deposit")
                        //{
                        //    return RedirectToAction("IndexPaymentDeposit");
                        //}

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message;
                    }
                }
            }


            //ViewBag.fk_id_customer = new SelectList(db.customers, "pk_id_customer", "name", order_total.fk_id_customer);
            return View(order_total);
        }

        // GET: payment/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_payment tbl_payment = db.tbl_payment.Find(id);
            if (tbl_payment == null)
            {
                return HttpNotFound();
            }
            return View(tbl_payment);
        }

        // POST: payment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_payment tbl_payment = db.tbl_payment.Find(id);
                    tbl_customerOrder customerOrder = db.tbl_customerOrder.Find(tbl_payment.fk_id_customerOrder);
                    var order = db.tbl_order.Where(item => item.fk_customer_id == customerOrder.pk_id_customerOrder);
                    if (customerOrder != null)
                    {
                        if (order != null)
                        {
                            foreach (var item in order)
                            {
                                db.tbl_order.Remove(item);
                            }
                        }
                        db.tbl_customerOrder.Remove(customerOrder);

                    }

                    db.tbl_payment.Remove(tbl_payment);
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback(); ViewData["Data"] = ex.Message;
                }
            }

            return RedirectToAction("Index", "payment");
        }

        public ActionResult Done(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_payment tbl_payment = db.tbl_payment.Find(id);
            tbl_payment.order_status = "Done";
            db.Entry(tbl_payment).State = EntityState.Modified;
            db.SaveChanges();
            //---------------------------------------------------------------

            var email = db.tbl_customerOrder.Where(item => item.pk_id_customerOrder == tbl_payment.fk_id_customerOrder).FirstOrDefault();
            var order = db.tbl_order.Where(item => item.fk_customer_id == email.pk_id_customerOrder);
            string data =
                "Dear " + email.name + "<br/>" +
                "Your Order is Done, Here's Your Details Order <br/>" +
                "===========================================<br/>" +
                "No. Payment : "+tbl_payment.pk_id_payment+"<br/>" +
                "Total Price : "+tbl_payment.total_price+"<br/>" +
                "Your Payment : "+tbl_payment.payment+"<br/>" +
                "Date : "+tbl_payment.date+ "<br/><br/>" +
                "Your Product : <br/>";
            foreach (var item in order)
            {
                data += 
                    "Product Name"+ item.tbl_product.product_name+"<br/>" +
                    "Width : "+item.width_meter+" Lenght : "+item.lenght_meter+"<br/>" +
                    "Amount : "+item.amount+"<br/>" +
                    "Details : "+item.details_order+"<br/>" +
                    "-----------------------------------------<br/>";
            }

            data +=
                "Please pick Your Order Soon<br/>" +
                "Thank you for your Order! We Wait For Your Next Visit<br/>" +
                "===============================================================<br/>";
            var fromEmail = new MailAddress("periofficial.company@gmail.com", "PERI");
            var toEmail = new MailAddress(email.email);
            var fromEmailPassword = "logPeri987"; // Replace with actual password
            

            string subject = "";
            string body = "";
            subject = "Your Order is Done!";
            body = data;



            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var msg = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true

            })
                smtp.Send(msg);

            //---------------------------------------------------------------------





            if (tbl_payment == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_customerOrder = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", tbl_payment.fk_id_customerOrder);
            return RedirectToAction("IndexDone","payment");
        }

        public ActionResult OnProgress(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_payment tbl_payment = db.tbl_payment.Find(id);
            TempData["customername"] = tbl_payment.tbl_customerOrder.name;
            if (tbl_payment == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_customerOrder = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", tbl_payment.fk_id_customerOrder);
            return View(tbl_payment);
        }

        [HttpPost, ActionName("OnProgress")]
        [ValidateAntiForgeryToken]
        public ActionResult OnProgress(tbl_payment tbl_payment)
        {
            if (ModelState.IsValid)
            {
                tbl_payment.order_status = "On Progress";
                db.Entry(tbl_payment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexOnProgress", "payment");
            }
            return View(tbl_payment);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}
