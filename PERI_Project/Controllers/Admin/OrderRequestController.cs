﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Admin
{
    public class OrderRequestController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: OrderRequest
        public ActionResult Index()
        {
            var request = db.tbl_OrderRequest.Where(item => item.status == "Not Yet").OrderByDescending(item => item.pk_id_OrderRequest);
            return View(request.ToList());
        }
        public ActionResult IndexDone()
        {
            var request = db.tbl_OrderRequest.Where(item=>item.status=="Done").OrderByDescending(item => item.pk_id_OrderRequest);
            return View(request.ToList());
        }
        // GET: OrderRequest/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_OrderRequest tbl_OrderRequest = db.tbl_OrderRequest.Find(id);
            if (tbl_OrderRequest == null)
            {
                return HttpNotFound();
            }
            return View(tbl_OrderRequest);
        }

        // GET: OrderRequest/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OrderRequest/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_OrderRequest,Name,address,phone,email,description,status")] tbl_OrderRequest tbl_OrderRequest)
        {
            if (ModelState.IsValid)
            {
                db.tbl_OrderRequest.Add(tbl_OrderRequest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_OrderRequest);
        }

        // GET: OrderRequest/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_OrderRequest tbl_OrderRequest = db.tbl_OrderRequest.Find(id);
            Session["Request"] = tbl_OrderRequest;
            tbl_customerOrder customerOrder = new tbl_customerOrder();
            customerOrder.name = tbl_OrderRequest.Name;
            customerOrder.phone = tbl_OrderRequest.phone;
            customerOrder.email = tbl_OrderRequest.email;
            db.tbl_customerOrder.Add(customerOrder);
            tbl_OrderRequest.status = "Done";
            db.Entry(tbl_OrderRequest).State = EntityState.Modified;
            db.SaveChanges();
            Session["Customer"] = customerOrder;
            if (tbl_OrderRequest == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("Create", "order");
        }
        
        // POST: OrderRequest/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_OrderRequest,Name,address,phone,email,description,status")] tbl_OrderRequest tbl_OrderRequest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_OrderRequest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_OrderRequest);
        }

        // GET: OrderRequest/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_OrderRequest tbl_OrderRequest = db.tbl_OrderRequest.Find(id);
            if (tbl_OrderRequest == null)
            {
                return HttpNotFound();
            }
            return View(tbl_OrderRequest);
        }

        // POST: OrderRequest/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_OrderRequest tbl_OrderRequest = db.tbl_OrderRequest.Find(id);
            db.tbl_OrderRequest.Remove(tbl_OrderRequest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Done(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_OrderRequest tbl_OrderRequest = db.tbl_OrderRequest.Find(id);
            tbl_OrderRequest.status = "Done";
            db.Entry(tbl_OrderRequest).State = EntityState.Modified;
            db.SaveChanges();
            if (tbl_OrderRequest == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
