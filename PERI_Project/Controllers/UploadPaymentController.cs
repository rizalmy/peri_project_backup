﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;
using System.IO;

namespace PERI_Project.Controllers
{
    public class UploadPaymentController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: UploadPayment
        public ActionResult Index()
        {
            return View(db.tbl_UploadPayment.ToList().OrderBy(item => item.pk_id_UploadPayment));
        }

        // GET: UploadPayment/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_UploadPayment tbl_UploadPayment = db.tbl_UploadPayment.Find(id);
            if (tbl_UploadPayment == null)
            {
                return HttpNotFound();
            }
            return View(tbl_UploadPayment);
        }

        // GET: UploadPayment/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UploadPayment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_UploadPayment UploadPayment)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    var isNull = isPaymentNUll(UploadPayment.payment_id);
                    var isExist = isUploadExist(UploadPayment.payment_id);
                    if (isNull)
                    {
                        TempData["Error"] = "Your Payment ID doesn't Exist, Please Check again!";
                        return RedirectToAction("Upload", "HomeCustomer");
                    }

                    if (isExist)
                    {
                        TempData["Error"] = "Your Upload Already Exist, Please Contact The Company for further Info!";
                        return RedirectToAction("Upload", "HomeCustomer");
                    }

                    string filename = Path.GetFileNameWithoutExtension(UploadPayment.ImageFile.FileName);
                    string extension = Path.GetExtension(UploadPayment.ImageFile.FileName);
                    filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                    UploadPayment.ImagePath = "~/Content/images/payment_proof/" + filename;
                    filename = Path.Combine(Server.MapPath("~/Content/images/payment_proof/"), filename);


                    if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
                    {
                        if (UploadPayment.ImageFile.ContentLength <= 100000)
                        {
                            db.tbl_UploadPayment.Add(UploadPayment);
                            if (db.SaveChanges() > 0)
                            {
                                UploadPayment.ImageFile.SaveAs(filename);
                                TempData["msg"] = "Success Upload Payment Proof";
                                ModelState.Clear();
                                return RedirectToAction("HomePage", "HomeCustomer");
                            }
                        }
                        else
                        {
                            ViewBag.msg = "File Size must be Equal or less than 100kb";
                        }
                    }
                    else
                    {
                        ViewBag.msg = "Invalid File Type ! Select file type [ jpg or png ]";
                    }
                }
                catch (Exception error)
                {
                    throw error;
                }
                
            }

            return View(UploadPayment);
        }

        // GET: UploadPayment/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_UploadPayment UploadPayment = db.tbl_UploadPayment.Find(id);
            tbl_payment payment = db.tbl_payment.Where(item => item.pk_id_payment == UploadPayment.payment_id).FirstOrDefault();

            payment.BankAccountName = UploadPayment.BankAccountName;
            payment.BankAccountNumber = UploadPayment.BankAccountNumber;
            payment.BankName = UploadPayment.BankName;
            payment.ImagePath = UploadPayment.ImagePath;
            db.Entry(payment).State = EntityState.Modified;
            db.tbl_UploadPayment.Remove(UploadPayment);
            db.SaveChanges();
            if (UploadPayment == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("Index","payment");
        }

        // POST: UploadPayment/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_UploadPayment,payment_id,ImagePath,BankName,BankAccountName,BankAccountNumber")] tbl_UploadPayment tbl_UploadPayment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_UploadPayment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_UploadPayment);
        }

        // GET: UploadPayment/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_UploadPayment tbl_UploadPayment = db.tbl_UploadPayment.Find(id);
            if (tbl_UploadPayment == null)
            {
                return HttpNotFound();
            }
            return View(tbl_UploadPayment);
        }

        // POST: UploadPayment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var Upload = db.tbl_UploadPayment.Find(id);
                    string currentImg = Request.MapPath(Upload.ImagePath);
                    db.Entry(Upload).State = EntityState.Deleted;
                    if (db.SaveChanges() > 0)
                    {
                        if (System.IO.File.Exists(currentImg))
                        {
                            System.IO.File.Delete(currentImg);
                        }
                        TempData["msg"] = "Successfully deleted data payment proof";
                        transaction.Commit();
                        return RedirectToAction("Index", "UploadPayment");
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ViewData["error"] = ex.Message;
                }
            }
            return RedirectToAction("Index", "UploadPayment");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [NonAction]
        public bool isPaymentNUll(int? payment_id)
        {
            using (PERIEntityDB dc = new PERIEntityDB())
            {
                tbl_payment payment = db.tbl_payment.Where(item => item.pk_id_payment == payment_id).FirstOrDefault();
                return payment == null;
            }
        }

        [NonAction]
        public bool isUploadExist(int? payment_id)
        {
            using (PERIEntityDB dc = new PERIEntityDB())
            {
                tbl_UploadPayment Upload = db.tbl_UploadPayment.Where(item => item.payment_id == payment_id).FirstOrDefault();
                //tbl_payment payment = db.tbl_payment.Where(item => item.pk_id_payment == payment_id).FirstOrDefault();
                return Upload != null;
            }
        }
    }
}
