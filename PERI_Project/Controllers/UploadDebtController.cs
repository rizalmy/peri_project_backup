﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;
using System.IO;
namespace PERI_Project.Controllers
{
    public class UploadDebtController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: UploadDebt
        public ActionResult Index()
        {
            return View(db.tbl_UploadDebt.ToList());
        }

        // GET: UploadDebt/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_UploadDebt tbl_UploadDebt = db.tbl_UploadDebt.Find(id);
            if (tbl_UploadDebt == null)
            {
                return HttpNotFound();
            }
            return View(tbl_UploadDebt);
        }

        // GET: UploadDebt/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UploadDebt/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_UploadDebt tbl_UploadDebt)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var isNull = isPaymentNUll(tbl_UploadDebt.Debt_id);
                    var isExist = isUploadExist(tbl_UploadDebt.Debt_id);

                    if (isNull)
                    {
                        TempData["Error"] = "Your Debt ID doesn't Exist, Please Check again!";
                        return RedirectToAction("UploadDebt", "HomeCustomer");
                    }

                    if (isExist)
                    {
                        TempData["Error"] = "Your Upload Debt Already Exist, Please Contact The Company for further Info!";
                        return RedirectToAction("UploadDebt", "HomeCustomer");
                    }
                    string filename = Path.GetFileNameWithoutExtension(tbl_UploadDebt.ImageFile.FileName);
                    string extension = Path.GetExtension(tbl_UploadDebt.ImageFile.FileName);
                    filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                    tbl_UploadDebt.ImagePath = "~/Content/images/debt_proof/" + filename;
                    filename = Path.Combine(Server.MapPath("~/Content/images/debt_proof/"), filename);

                    if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
                    {
                        if (tbl_UploadDebt.ImageFile.ContentLength <= 100000)
                        {
                            db.tbl_UploadDebt.Add(tbl_UploadDebt);
                            if (db.SaveChanges() > 0)
                            {
                                tbl_UploadDebt.ImageFile.SaveAs(filename);
                                TempData["msg"] = "Success Upload Debt Payment Proof";
                                ModelState.Clear();
                                return RedirectToAction("HomePage", "HomeCustomer");
                            }
                        }
                        else
                        {
                            ViewBag.msg = "File Size must be Equal or less than 100kb";
                        }
                    }
                    else
                    {
                        ViewBag.msg = "Invalid File Type ! Select file type [ jpg or png ]";
                    }
                }
                catch (Exception error) { throw error; }
     

            }

            return View(tbl_UploadDebt);
        }

        // GET: UploadDebt/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_UploadDebt tbl_UploadDebt = db.tbl_UploadDebt.Find(id);
            tbl_customerDebt tbl_CustomerDebt = db.tbl_customerDebt.Where(item=>item.pk_id_cDebt==tbl_UploadDebt.Debt_id).FirstOrDefault();
 
            tbl_CustomerDebt.BankAccountName = tbl_UploadDebt.BankAccountName;
            tbl_CustomerDebt.BankAccountNumber = tbl_UploadDebt.BankAccountNumber;
            tbl_CustomerDebt.BankName = tbl_UploadDebt.BankName;
            tbl_CustomerDebt.ImagePath = tbl_UploadDebt.ImagePath;
            db.Entry(tbl_CustomerDebt).State = EntityState.Modified;
            db.tbl_UploadDebt.Remove(tbl_UploadDebt);
            db.SaveChanges();
            if (tbl_UploadDebt == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("Index", "customerDebt");
        }

        // POST: UploadDebt/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_UploadDebt,Debt_id,ImagePath,BankName,BankAccountName,BankAccountNumber")] tbl_UploadDebt tbl_UploadDebt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_UploadDebt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_UploadDebt);
        }

        // GET: UploadDebt/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_UploadDebt tbl_UploadDebt = db.tbl_UploadDebt.Find(id);
            if (tbl_UploadDebt == null)
            {
                return HttpNotFound();
            }
            return View(tbl_UploadDebt);
        }

        // POST: UploadDebt/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_UploadDebt tbl_UploadDebt = db.tbl_UploadDebt.Find(id);
            db.tbl_UploadDebt.Remove(tbl_UploadDebt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [NonAction]
        public bool isPaymentNUll(int? payment_id)
        {
            using (PERIEntityDB dc = new PERIEntityDB())
            {
                tbl_customerDebt debt = db.tbl_customerDebt.Where(item => item.pk_id_cDebt == payment_id).FirstOrDefault();
                return debt == null;
            }
        }

        [NonAction]
        public bool isUploadExist(int? payment_id)
        {
            using (PERIEntityDB dc = new PERIEntityDB())
            {
                tbl_UploadDebt debt = db.tbl_UploadDebt.Where(item => item.Debt_id == payment_id).FirstOrDefault();
                return debt != null;
            }
        }



    }
}
