﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class HistoryCustomerDebtController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: HistoryCustomerDebt
        public ActionResult Index(int? id)
        {
            var tbl_HistoryCustomerDebt = db.tbl_HistoryCustomerDebt.Include(t => t.tbl_customerDebt).Where(item=>item.fk_id_CustomerDebt==id).OrderByDescending(item => item.pk_id_HistoryCustomerDebt);
            return View(tbl_HistoryCustomerDebt.ToList());
        }

        // GET: HistoryCustomerDebt/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_HistoryCustomerDebt tbl_HistoryCustomerDebt = db.tbl_HistoryCustomerDebt.Find(id);
            if (tbl_HistoryCustomerDebt == null)
            {
                return HttpNotFound();
            }
            return View(tbl_HistoryCustomerDebt);
        }

        // GET: HistoryCustomerDebt/Create
        public ActionResult Create()
        {
            ViewBag.fk_id_CustomerDebt = new SelectList(db.tbl_customerDebt, "pk_id_cDebt", "debt_status");
            return View();
        }

        // POST: HistoryCustomerDebt/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_HistoryCustomerDebt,fk_id_CustomerDebt,payment,date")] tbl_HistoryCustomerDebt tbl_HistoryCustomerDebt)
        {
            if (ModelState.IsValid)
            {
                db.tbl_HistoryCustomerDebt.Add(tbl_HistoryCustomerDebt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fk_id_CustomerDebt = new SelectList(db.tbl_customerDebt, "pk_id_cDebt", "debt_status", tbl_HistoryCustomerDebt.fk_id_CustomerDebt);
            return View(tbl_HistoryCustomerDebt);
        }

        // GET: HistoryCustomerDebt/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_HistoryCustomerDebt tbl_HistoryCustomerDebt = db.tbl_HistoryCustomerDebt.Find(id);
            if (tbl_HistoryCustomerDebt == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_CustomerDebt = new SelectList(db.tbl_customerDebt, "pk_id_cDebt", "debt_status", tbl_HistoryCustomerDebt.fk_id_CustomerDebt);
            return View(tbl_HistoryCustomerDebt);
        }

        // POST: HistoryCustomerDebt/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_HistoryCustomerDebt,fk_id_CustomerDebt,payment,date")] tbl_HistoryCustomerDebt tbl_HistoryCustomerDebt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_HistoryCustomerDebt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fk_id_CustomerDebt = new SelectList(db.tbl_customerDebt, "pk_id_cDebt", "debt_status", tbl_HistoryCustomerDebt.fk_id_CustomerDebt);
            return View(tbl_HistoryCustomerDebt);
        }

        // GET: HistoryCustomerDebt/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_HistoryCustomerDebt tbl_HistoryCustomerDebt = db.tbl_HistoryCustomerDebt.Find(id);
            if (tbl_HistoryCustomerDebt == null)
            {
                return HttpNotFound();
            }
            return View(tbl_HistoryCustomerDebt);
        }

        // POST: HistoryCustomerDebt/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_HistoryCustomerDebt tbl_HistoryCustomerDebt = db.tbl_HistoryCustomerDebt.Find(id);
            db.tbl_HistoryCustomerDebt.Remove(tbl_HistoryCustomerDebt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
