﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class Owner_debtController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: Owner_debt
        public ActionResult Index()
        {
            var tbl_Owner_debt = db.tbl_Owner_debt.Include(t => t.tbl_spending).OrderByDescending(item => item.pk_id_oDebt);
            return View(tbl_Owner_debt.ToList());
        }

        // GET: Owner_debt/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Owner_debt tbl_Owner_debt = db.tbl_Owner_debt.Find(id);
            if (tbl_Owner_debt == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("Index","HistoryOwnerDebt",new { id = id });
        }

        // GET: Owner_debt/Create
        public ActionResult Create()
        {
            ViewBag.fk_id_spending = new SelectList(db.tbl_spending, "pk_id_spending", "payment_status");
            return View();
        }

        // POST: Owner_debt/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_oDebt,fk_id_spending,debt_status,date,payment,debt")] tbl_Owner_debt tbl_Owner_debt)
        {
            if (ModelState.IsValid)
            {
                db.tbl_Owner_debt.Add(tbl_Owner_debt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fk_id_spending = new SelectList(db.tbl_spending, "pk_id_spending", "payment_status", tbl_Owner_debt.fk_id_spending);
            return View(tbl_Owner_debt);
        }

        // GET: Owner_debt/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Owner_debt tbl_Owner_debt = db.tbl_Owner_debt.Find(id);
            if (tbl_Owner_debt == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_spending = new SelectList(db.tbl_spending, "pk_id_spending", "payment_status", tbl_Owner_debt.fk_id_spending);
            return View(tbl_Owner_debt);
        }

        // POST: Owner_debt/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_oDebt,fk_id_spending,debt_status,date,payment,debt")] tbl_Owner_debt tbl_Owner_debt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Owner_debt).State = EntityState.Modified;
                tbl_Owner_debt.date = DateTime.Now;
                tbl_spending spending = db.tbl_spending.Find(tbl_Owner_debt.fk_id_spending);
                spending.payment += tbl_Owner_debt.payment;
                if (spending.payment > spending.tbl_spendingItem.price) 
                {
                    tbl_depositSpending deposit = new tbl_depositSpending();
                    deposit.fk_id_spending = spending.pk_id_spending;
                    deposit.deposit = spending.payment - spending.tbl_spendingItem.price;
                    deposit.date = DateTime.Now;
                    //deposit.date = tbl_Owner_debt.date;
                    db.tbl_depositSpending.Add(deposit);
                    spending.payment_status = "Deposit";
                    //spending.payment = spending.tbl_spendingItem.price;
                }
               else if (spending.payment == spending.tbl_spendingItem.price)
                {
                    tbl_Owner_debt.debt_status = "Done";
                    spending.payment_status = "Done";
                }
                tbl_Owner_debt.debt -= tbl_Owner_debt.payment;
                if (tbl_Owner_debt.debt < 0) 
                {
                    tbl_Owner_debt.debt = 0;
                }
                tbl_HistoryOwnerDebt HistoryDebt = new tbl_HistoryOwnerDebt();
                HistoryDebt.fk_id_OwnerDebt = tbl_Owner_debt.pk_id_oDebt;
                HistoryDebt.payment = tbl_Owner_debt.payment;
                HistoryDebt.date = DateTime.Now;
                //HistoryDebt.date = tbl_Owner_debt.date;
                db.tbl_HistoryOwnerDebt.Add(HistoryDebt);
                tbl_Owner_debt.payment = null;
                db.Entry(tbl_Owner_debt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fk_id_spending = new SelectList(db.tbl_spending, "pk_id_spending", "payment_status", tbl_Owner_debt.fk_id_spending);
            return View(tbl_Owner_debt);
        }

        // GET: Owner_debt/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Owner_debt tbl_Owner_debt = db.tbl_Owner_debt.Find(id);
            if (tbl_Owner_debt == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Owner_debt);
        }

        // POST: Owner_debt/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Owner_debt tbl_Owner_debt = db.tbl_Owner_debt.Find(id);
            db.tbl_Owner_debt.Remove(tbl_Owner_debt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
