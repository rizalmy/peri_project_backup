﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class spendingItemController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: spendingItem
        public ActionResult Index()
        {
            return View(db.tbl_spendingItem.ToList());
        }

        // GET: spendingItem/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_spendingItem tbl_spendingItem = db.tbl_spendingItem.Find(id);
            if (tbl_spendingItem == null)
            {
                return HttpNotFound();
            }
            return View(tbl_spendingItem);
        }

        // GET: spendingItem/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: spendingItem/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_spendingItem,name,price")] tbl_spendingItem tbl_spendingItem)
        {
            if (ModelState.IsValid)
            {
                db.tbl_spendingItem.Add(tbl_spendingItem);
                db.SaveChanges();
                return RedirectToAction("Create", "spending");
            }

            return View(tbl_spendingItem);
        }

        // GET: spendingItem/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_spendingItem tbl_spendingItem = db.tbl_spendingItem.Find(id);
            if (tbl_spendingItem == null)
            {
                return HttpNotFound();
            }
            return View(tbl_spendingItem);
        }

        // POST: spendingItem/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_spendingItem,name,price")] tbl_spendingItem tbl_spendingItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_spendingItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_spendingItem);
        }

        // GET: spendingItem/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_spendingItem tbl_spendingItem = db.tbl_spendingItem.Find(id);
            if (tbl_spendingItem == null)
            {
                return HttpNotFound();
            }
            return View(tbl_spendingItem);
        }

        // POST: spendingItem/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_spendingItem tbl_spendingItem = db.tbl_spendingItem.Find(id);
                    db.tbl_spendingItem.Remove(tbl_spendingItem);
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    transaction.Rollback(); ViewData["Data"] = ex.Message;
                }
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
