﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;
using System.IO;
namespace PERI_Project.Controllers.Owner
{
    public class productController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: product
        public ActionResult Index()
        {
            return View(db.tbl_product.ToList().OrderByDescending(item => item.pk_id_product));
        }

        // GET: product/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_product tbl_product = db.tbl_product.Find(id);
            if (tbl_product == null)
            {
                return HttpNotFound();
            }
            return View(tbl_product);
        }

        // GET: product/Create
        public ActionResult Create(int id = 0)
        {
            tbl_product product = new tbl_product();
            return View(product);
        }

        // POST: product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(/*[Bind(Include = "pk_id_product,product_name,product_price")]*/ tbl_product product)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        string filename = Path.GetFileNameWithoutExtension(product.ImageFile.FileName);
                        string extension = Path.GetExtension(product.ImageFile.FileName);
                        filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                        product.ImagePath = "~/Content/images/products/" + filename;
                        filename = Path.Combine(Server.MapPath("~/Content/images/products/"), filename);

                        if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
                        {
                            if (product.ImageFile.ContentLength <= 500000)
                            {
                                db.tbl_product.Add(product);

                                if (db.SaveChanges() > 0)
                                {
                                    product.ImageFile.SaveAs(filename);
                                    TempData["msg"] = "Success data product added";
                                    ModelState.Clear();
                                }
                                transaction.Commit();
                            }
                            else
                            {
                                ViewBag.msg = "File Size must be Equal or less than 500kb";
                            }
                        }
                        else
                        {
                            ViewBag.msg = "Invalid File Type ! Select file type [ jpg or png ]";
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        ViewData["error"] = ex.Message;
                    }
                }
            }
            return RedirectToAction("Index", "product");
        }

        // GET: product/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = db.tbl_product.Find(id);
            Session["imgPath"] = product.ImagePath;
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_product product)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (product.ImageFile != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(product.ImageFile.FileName);
                        string extension = Path.GetExtension(product.ImageFile.FileName);
                        filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                        product.ImagePath = "~/Content/images/products/" + filename;
                        filename = Path.Combine(Server.MapPath("~/Content/images/products/"), filename);

                        if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
                        {
                            if (product.ImageFile.ContentLength <= 500000)
                            {
                                db.Entry(product).State = EntityState.Modified;
                                string oldImgPath = Request.MapPath(Session["imgPath"].ToString());

                                if (db.SaveChanges() > 0)
                                {
                                    product.ImageFile.SaveAs(filename);

                                    if (System.IO.File.Exists(oldImgPath))
                                    {
                                        System.IO.File.Delete(oldImgPath);
                                    }
                                    TempData["msg"] = "Successful product data updated";
                                    return RedirectToAction("Index", "product");
                                }

                            }
                            else
                            {
                                ViewBag.msg = "File Size must be Equal or less than 500kb";
                            }
                        }
                        else
                        {
                            ViewBag.msg = "Invalid File Type ! Select file type [ jpg or png ]";
                        }
                    }
                    else
                    {
                        product.ImagePath = Session["imgPath"].ToString();
                        db.Entry(product).State = EntityState.Modified;
                        if (db.SaveChanges() > 0)
                        {
                            TempData["msg"] = "Successful product data updated";
                            return RedirectToAction("Index", "product");
                        }
                    }
                }
                catch (Exception error)
                {
                    throw error;
                }
            }
            return View();
        }

        // GET: product/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = db.tbl_product.Find(id);

            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var product = db.tbl_product.Find(id);
                    string currentImg = Request.MapPath(product.ImagePath);
                    db.Entry(product).State = EntityState.Deleted;
                    if (db.SaveChanges() > 0)
                    {
                        if (System.IO.File.Exists(currentImg))
                        {
                            System.IO.File.Delete(currentImg);
                        }
                        TempData["msg"] = "Successfully deleted product data";
                        transaction.Commit();
                        return RedirectToAction("Index", "product");
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ViewData["error"] = ex.Message;
                }
            }
            return RedirectToAction("Index", "product");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
