﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class depositPaymentController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: depositPayment
        public ActionResult Index()
        {
            var tbl_depositPayment = db.tbl_depositPayment.Include(t => t.tbl_payment).OrderByDescending(item => item.pk_id_depositPayment);
            return View(tbl_depositPayment.ToList());
        }

        // GET: depositPayment/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_depositPayment tbl_depositPayment = db.tbl_depositPayment.Find(id);
            if (tbl_depositPayment == null)
            {
                return HttpNotFound();
            }
            return View(tbl_depositPayment);
        }

        // GET: depositPayment/Create
        public ActionResult Create()
        {
            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status");
            return View();
        }

        // POST: depositPayment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_depositPayment,fk_id_payment,deposit,date")] tbl_depositPayment tbl_depositPayment)
        {
            if (ModelState.IsValid)
            {
                db.tbl_depositPayment.Add(tbl_depositPayment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status", tbl_depositPayment.fk_id_payment);
            return View(tbl_depositPayment);
        }

        // GET: depositPayment/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_depositPayment tbl_depositPayment = db.tbl_depositPayment.Find(id);
            if (tbl_depositPayment == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status", tbl_depositPayment.fk_id_payment);
            return View(tbl_depositPayment);
        }

        // POST: depositPayment/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_depositPayment,fk_id_payment,deposit,date")] tbl_depositPayment tbl_depositPayment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_depositPayment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status", tbl_depositPayment.fk_id_payment);
            return View(tbl_depositPayment);
        }

        // GET: depositPayment/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_depositPayment tbl_depositPayment = db.tbl_depositPayment.Find(id);
            if (tbl_depositPayment == null)
            {
                return HttpNotFound();
            }
            return View(tbl_depositPayment);
        }

        // POST: depositPayment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_depositPayment tbl_depositPayment = db.tbl_depositPayment.Find(id);
            tbl_payment payment = db.tbl_payment.Where(item => item.pk_id_payment == tbl_depositPayment.fk_id_payment).FirstOrDefault();
            payment.payment -= tbl_depositPayment.deposit;
            if (payment.payment == payment.total_price) 
            {
                payment.payment_status = "Done";
            }
            db.Entry(payment).State = EntityState.Modified;
            db.tbl_depositPayment.Remove(tbl_depositPayment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
