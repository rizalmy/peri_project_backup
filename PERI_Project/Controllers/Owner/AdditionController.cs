﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class AdditionController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: Addition
        public ActionResult Index()
        {
            return View(db.tbl_Addition.ToList());
        }

        // GET: Addition/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Addition tbl_Addition = db.tbl_Addition.Find(id);
            if (tbl_Addition == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Addition);
        }

        // GET: Addition/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Addition/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_addition,name,price")] tbl_Addition tbl_Addition)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.tbl_Addition.Add(tbl_Addition);
                        db.SaveChanges();
                        return RedirectToAction("Index");

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message;
                    }
                }

            }

            return View(tbl_Addition);
        }

        // GET: Addition/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Addition tbl_Addition = db.tbl_Addition.Find(id);
            if (tbl_Addition == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Addition);
        }

        // POST: Addition/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_addition,name,price")] tbl_Addition tbl_Addition)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.Entry(tbl_Addition).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message;
                    }
                }
            }
            return View(tbl_Addition);
        }

        // GET: Addition/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Addition tbl_Addition = db.tbl_Addition.Find(id);
            if (tbl_Addition == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Addition);
        }

        // POST: Addition/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_Addition tbl_Addition = db.tbl_Addition.Find(id);
                    db.tbl_Addition.Remove(tbl_Addition);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    transaction.Rollback(); ViewData["Data"] = ex.Message;
                }
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
