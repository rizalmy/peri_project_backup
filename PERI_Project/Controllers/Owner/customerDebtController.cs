﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class customerDebtController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: customerDebt
        public ActionResult Index()
        {
            var tbl_customerDebt = db.tbl_customerDebt.Include(t => t.tbl_payment).OrderByDescending(item => item.pk_id_cDebt);
            return View(tbl_customerDebt.ToList());
        }

        // GET: customerDebt/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_customerDebt tbl_customerDebt = db.tbl_customerDebt.Find(id);
            if (tbl_customerDebt == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("Index","HistoryCustomerDebt",new { id = id });
        }

        // GET: customerDebt/Create
        public ActionResult Create()
        {
            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status");
            return View();
        }

        // POST: customerDebt/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_cDebt,fk_id_payment,Debt,debt_status,payment, date")] tbl_customerDebt tbl_customerDebt)
        {
            if (ModelState.IsValid)
            {
                db.tbl_customerDebt.Add(tbl_customerDebt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status", tbl_customerDebt.fk_id_payment);
            return View(tbl_customerDebt);
        }

        // GET: customerDebt/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_customerDebt tbl_customerDebt = db.tbl_customerDebt.Find(id);
            if (tbl_customerDebt == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status", tbl_customerDebt.fk_id_payment);
            return View(tbl_customerDebt);
        }

        // POST: customerDebt/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_customerDebt tbl_customerDebt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_customerDebt).State = EntityState.Modified;
                tbl_customerDebt.date = DateTime.Now;
                tbl_payment payment = db.tbl_payment.Find(tbl_customerDebt.fk_id_payment);
                payment.payment += tbl_customerDebt.payment;
                if (payment.payment > payment.total_price) 
                {
                    tbl_depositPayment deposit = new tbl_depositPayment();
                    deposit.fk_id_payment = payment.pk_id_payment;
                    deposit.deposit = payment.payment - payment.total_price;
                    //deposit.date = DateTime.Now;
                    deposit.date = tbl_customerDebt.date;
                    db.tbl_depositPayment.Add(deposit);
                    payment.payment_status = "Deposit";
                    //payment.payment = payment.total_price;
                }
                else if (payment.payment == payment.total_price) 
                {
                    tbl_customerDebt.debt_status = "Done";
                    payment.payment_status = "Done";
                }
                tbl_customerDebt.Debt -= tbl_customerDebt.payment;
                if (tbl_customerDebt.Debt < 0) 
                {
                    tbl_customerDebt.Debt = 0;
                }
                db.Entry(tbl_customerDebt).State = EntityState.Modified;
                db.SaveChanges();
                tbl_HistoryCustomerDebt HistoryDebt = new tbl_HistoryCustomerDebt();
                HistoryDebt.date = DateTime.Now;
                HistoryDebt.fk_id_CustomerDebt = tbl_customerDebt.pk_id_cDebt;
                HistoryDebt.payment = tbl_customerDebt.payment;
                if (tbl_customerDebt.BankAccountName == null || tbl_customerDebt.BankAccountNumber == null ||
                    tbl_customerDebt.BankName == null || tbl_customerDebt.ImagePath == null)
                {
                    HistoryDebt.BankAccountName = "On the spot";
                    HistoryDebt.BankAccountNumber = "On the spot";
                    HistoryDebt.BankName = "On the spot";
                    HistoryDebt.ImagePath = "~/Content/images/debt_proof/invoice_default.png";
                }
                else
                {
                    HistoryDebt.BankAccountName = tbl_customerDebt.BankAccountName;
                    HistoryDebt.BankAccountNumber = tbl_customerDebt.BankAccountNumber;
                    HistoryDebt.BankName = tbl_customerDebt.BankName;
                    HistoryDebt.ImagePath = tbl_customerDebt.ImagePath;
                }


               //HistoryDebt.date = tbl_customerDebt.date;
                db.tbl_HistoryCustomerDebt.Add(HistoryDebt);
                tbl_customerDebt.payment = null;
                tbl_customerDebt.date = DateTime.Now;
                db.Entry(tbl_customerDebt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status", tbl_customerDebt.fk_id_payment);
            return View(tbl_customerDebt);
        }

        // GET: customerDebt/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_customerDebt tbl_customerDebt = db.tbl_customerDebt.Find(id);
            if (tbl_customerDebt == null)
            {
                return HttpNotFound();
            }
            return View(tbl_customerDebt);
        }

        // POST: customerDebt/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_customerDebt tbl_customerDebt = db.tbl_customerDebt.Find(id);
            db.tbl_customerDebt.Remove(tbl_customerDebt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
