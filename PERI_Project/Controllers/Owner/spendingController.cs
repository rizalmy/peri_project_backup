﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class spendingController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: spending
        public ActionResult Index()
        {
            var tbl_spending = db.tbl_spending.Include(t => t.tbl_spendingItem).OrderByDescending(item => item.pk_id_spending);
            return View(tbl_spending.ToList());
        }

        // GET: spending/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_spending tbl_spending = db.tbl_spending.Find(id);
            if (tbl_spending == null)
            {
                return HttpNotFound();
            }
            return View(tbl_spending);
        }

        // GET: spending/Create
        public ActionResult Create()
        {
            //ViewBag.fk_id_spendingItem = new SelectList(db.tbl_spendingItem, "pk_id_spendingItem", "name");
            //ViewBag.priceItem = new SelectList(db.tbl_spendingItem, "pk_id_spendingItem", "price");
            return View();
        }
        public ActionResult GetData()
        {
            var data = from als in db.tbl_spendingItem select new { als.name, als.price, als.pk_id_spendingItem };
            return Json(data.ToList(), JsonRequestBehavior.AllowGet);
        }


        // POST: spending/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_spending spending)
        {
            if (ModelState.IsValid)
            {
                tbl_spendingItem item = db.tbl_spendingItem.Find(spending.fk_id_spendingItem);
                spending.date = DateTime.Now;
                if (spending.payment > item.price)
                {
                    tbl_depositSpending deposit = new tbl_depositSpending();
                    deposit.fk_id_spending = spending.pk_id_spending;
                    deposit.deposit = spending.payment - item.price;
                    deposit.date = DateTime.Now;
                    //deposit.date = tbl_spending.date;
                    db.tbl_depositSpending.Add(deposit);
                    spending.payment_status = "Deposit";
                    // tbl_spending.payment = item.price;
                }
                else if (spending.payment == item.price)
                {
                    spending.payment_status = "Done";
                }
                else
                {
                    spending.payment_status = "Debt";
                    tbl_Owner_debt debt = new tbl_Owner_debt();
                    debt.date = DateTime.Now;
                    debt.fk_id_spending = spending.pk_id_spending;
                    debt.debt = item.price - spending.payment;
                    debt.debt_status = "Not Yet";
                    db.tbl_Owner_debt.Add(debt);
                }

                db.tbl_spending.Add(spending);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fk_id_spendingItem = new SelectList(db.tbl_spendingItem, "pk_id_spendingItem", "name", spending.fk_id_spendingItem);
            return View(spending);
        }

        // GET: spending/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_spending tbl_spending = db.tbl_spending.Find(id);
            if (tbl_spending == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_spendingItem = new SelectList(db.tbl_spendingItem, "pk_id_spendingItem", "name", tbl_spending.fk_id_spendingItem);
            return View(tbl_spending);
        }

        // POST: spending/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_spending,payment,date,fk_id_spendingItem")] tbl_spending tbl_spending)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_spending).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fk_id_spendingItem = new SelectList(db.tbl_spendingItem, "pk_id_spendingItem", "name", tbl_spending.fk_id_spendingItem);
            return View(tbl_spending);
        }

        // GET: spending/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_spending tbl_spending = db.tbl_spending.Find(id);
            if (tbl_spending == null)
            {
                return HttpNotFound();
            }
            return View(tbl_spending);
        }

        // POST: spending/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_spending tbl_spending = db.tbl_spending.Find(id);
            tbl_Owner_debt debt = db.tbl_Owner_debt.Where(item => item.fk_id_spending == tbl_spending.pk_id_spending).FirstOrDefault();
            var history = db.tbl_HistoryOwnerDebt.Where(item => item.fk_id_OwnerDebt == debt.pk_id_oDebt);

            if (debt != null)
            {
                if (history != null)
                {
                    foreach (var item in history)
                    {
                        db.tbl_HistoryOwnerDebt.Remove(item);
                    }
                }
                db.tbl_Owner_debt.Remove(debt);
            }
            //db.tbl_Owner_debt.Remove(debt);
            db.tbl_spending.Remove(tbl_spending);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
