﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Entity;
using System.Net;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class reportController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();
        // GET: report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Profit()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1025);
            report.Height = Unit.Pixel(760);
            string urlReportServer = "http://localhost/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/Profit";
            string name = "Profit_"+DateTime.Now.ToString();
        
           // string parameter=report.ServerReport.GetParameters().ToString();
            report.ServerReport.DisplayName = name;
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();

        }

        public ActionResult Income()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1025);
            report.Height = Unit.Pixel(760);
            string urlReportServer = "http://localhost/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/Income";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();

        }

        public ActionResult Spending()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1025);
            report.Height = Unit.Pixel(760);
            string urlReportServer = "http://localhost/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/Spending";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();

        }

        public ActionResult Transaction()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1025);
            report.Height = Unit.Pixel(760);
            string urlReportServer = "http://localhost/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/Transaction";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();

        }

        public ActionResult Debt()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1025);
            report.Height = Unit.Pixel(760);
            string urlReportServer = "http://localhost/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/Debt";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();

        }

        public ActionResult Product()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1025);
            report.Height = Unit.Pixel(760);
            string urlReportServer = "http://localhost/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/Product";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();

        }

        public ActionResult Deposit()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1025);
            report.Height = Unit.Pixel(760);
            string urlReportServer = "http://localhost/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/Deposit";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();

        }

        public ActionResult Overview()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1025);
            report.Height = Unit.Pixel(760);
            string urlReportServer = "http://localhost/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/Overview";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();

        }

        public ActionResult Receipt(int? id)
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1025);
            report.Height = Unit.Pixel(760);
            string urlReportServer = "http://localhost/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/Receipt";
            report.ServerReport.Refresh();


            var customerOrder =db.tbl_payment.Where(item=>item.pk_id_payment==id).FirstOrDefault();

            string ID_payment = id.ToString();
            string name = customerOrder.tbl_customerOrder.name+ID_payment+"_"+DateTime.Now.ToString();
            ReportParameter[] parameters = new ReportParameter[1];
            parameters[0] = new ReportParameter("paramID", ID_payment, true);
            report.ServerReport.SetParameters(parameters);
            report.ServerReport.Refresh();
            report.ServerReport.DisplayName =name;
            ViewBag.ReportViewer = report;
            return View();

        }
    }
}