﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class contactController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: contact
        public ActionResult Index()
        {
            return View(db.tbl_contact.ToList());
        }

        // GET: contact/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_contact tbl_contact = db.tbl_contact.Find(id);
            if (tbl_contact == null)
            {
                return HttpNotFound();
            }
            return View(tbl_contact);
        }

        // GET: contact/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: contact/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_contact,address,phone,email")] tbl_contact tbl_contact)
        {
            if (ModelState.IsValid)
            {
                db.tbl_contact.Add(tbl_contact);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_contact);
        }

        // GET: contact/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbl_contact = db.tbl_contact.Find(id);
            Session["imgPath"] = tbl_contact.Image;
            if (tbl_contact == null)
            {
                return HttpNotFound();
            }
            return View(tbl_contact);
        }

        // POST: contact/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_contact tbl_contact)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (tbl_contact.ImageFile != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(tbl_contact.ImageFile.FileName);
                        string extension = Path.GetExtension(tbl_contact.ImageFile.FileName);
                        filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                        tbl_contact.Image = "~/Content/images/contact/" + filename;
                        filename = Path.Combine(Server.MapPath("~/Content/images/contact/"), filename);

                        if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
                        {
                            if (tbl_contact.ImageFile.ContentLength <= 500000)
                            {
                                db.Entry(tbl_contact).State = EntityState.Modified;
                                string oldImgPath = Request.MapPath(Session["imgPath"].ToString());

                                if (db.SaveChanges() > 0)
                                {
                                    tbl_contact.ImageFile.SaveAs(filename);

                                    if (System.IO.File.Exists(oldImgPath))
                                    {
                                        System.IO.File.Delete(oldImgPath);
                                    }
                                    TempData["msg"] = "Successful contact data updated";
                                    return RedirectToAction("Index", "contact");
                                }
                            }
                            else
                            {
                                ViewBag.msg = "File Size must be Equal or less than 500kb";
                            }
                        }
                        else
                        {
                            ViewBag.msg = "Invalid File Type ! Select file type [ jpg or png ]";
                        }
                    }
                    else
                    {
                        tbl_contact.Image = Session["imgPath"].ToString();
                        db.Entry(tbl_contact).State = EntityState.Modified;
                        if (db.SaveChanges() > 0)
                        {
                            TempData["msg"] = "Successful contact data updated";
                            return RedirectToAction("Index", "contact");
                        }
                    }
                }
                catch (Exception error)
                {
                    throw error;
                }
               
            }
            return View();
        }

        // GET: contact/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_contact tbl_contact = db.tbl_contact.Find(id);
            if (tbl_contact == null)
            {
                return HttpNotFound();
            }
            return View(tbl_contact);
        }

        // POST: contact/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_contact tbl_contact = db.tbl_contact.Find(id);
            db.tbl_contact.Remove(tbl_contact);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
