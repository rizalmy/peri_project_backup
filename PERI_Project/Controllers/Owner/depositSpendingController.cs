﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class depositSpendingController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: depositSpending
        public ActionResult Index()
        {
            var tbl_depositSpending = db.tbl_depositSpending.Include(t => t.tbl_spending).OrderByDescending(item => item.pk_id_depositSpending);
            return View(tbl_depositSpending.ToList());
        }

        // GET: depositSpending/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_depositSpending tbl_depositSpending = db.tbl_depositSpending.Find(id);
            if (tbl_depositSpending == null)
            {
                return HttpNotFound();
            }
            return View(tbl_depositSpending);
        }

        // GET: depositSpending/Create
        public ActionResult Create()
        {
            ViewBag.fk_id_spending = new SelectList(db.tbl_spending, "pk_id_spending", "payment_status");
            return View();
        }

        // POST: depositSpending/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_depositSpending,fk_id_spending,deposit,date")] tbl_depositSpending tbl_depositSpending)
        {
            if (ModelState.IsValid)
            {
                db.tbl_depositSpending.Add(tbl_depositSpending);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fk_id_spending = new SelectList(db.tbl_spending, "pk_id_spending", "payment_status", tbl_depositSpending.fk_id_spending);
            return View(tbl_depositSpending);
        }

        // GET: depositSpending/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_depositSpending tbl_depositSpending = db.tbl_depositSpending.Find(id);
            if (tbl_depositSpending == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_spending = new SelectList(db.tbl_spending, "pk_id_spending", "payment_status", tbl_depositSpending.fk_id_spending);
            return View(tbl_depositSpending);
        }

        // POST: depositSpending/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_depositSpending,fk_id_spending,deposit,date")] tbl_depositSpending tbl_depositSpending)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_depositSpending).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fk_id_spending = new SelectList(db.tbl_spending, "pk_id_spending", "payment_status", tbl_depositSpending.fk_id_spending);
            return View(tbl_depositSpending);
        }

        // GET: depositSpending/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_depositSpending tbl_depositSpending = db.tbl_depositSpending.Find(id);
            if (tbl_depositSpending == null)
            {
                return HttpNotFound();
            }
            return View(tbl_depositSpending);
        }

        // POST: depositSpending/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_depositSpending tbl_depositSpending = db.tbl_depositSpending.Find(id);

            tbl_spending spending = db.tbl_spending.Where(item => item.pk_id_spending == tbl_depositSpending.fk_id_spending).FirstOrDefault();
            spending.payment -= tbl_depositSpending.deposit;
            if (spending.payment == spending.tbl_spendingItem.price)
            {
                spending.payment_status = "Done";
            }
            
            db.Entry(spending).State = EntityState.Modified;
            db.tbl_depositSpending.Remove(tbl_depositSpending);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
