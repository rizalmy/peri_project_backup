﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class otherSpendingController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: otherSpending
        public ActionResult Index()
        {
            return View(db.tbl_otherSpending.ToList());
        }

        // GET: otherSpending/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_otherSpending tbl_otherSpending = db.tbl_otherSpending.Find(id);
            if (tbl_otherSpending == null)
            {
                return HttpNotFound();
            }
            return View(tbl_otherSpending);
        }

        // GET: otherSpending/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: otherSpending/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_otherSpending,name,spending_value,date")] tbl_otherSpending tbl_otherSpending)
        {
            if (ModelState.IsValid)
            {
                tbl_otherSpending.date = DateTime.Now;
                db.tbl_otherSpending.Add(tbl_otherSpending);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_otherSpending);
        }

        // GET: otherSpending/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_otherSpending tbl_otherSpending = db.tbl_otherSpending.Find(id);
            if (tbl_otherSpending == null)
            {
                return HttpNotFound();
            }
            return View(tbl_otherSpending);
        }

        // POST: otherSpending/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_otherSpending,name,spending_value,date")] tbl_otherSpending tbl_otherSpending)
        {
            if (ModelState.IsValid)
            {
                tbl_otherSpending.date = DateTime.Now;
                db.Entry(tbl_otherSpending).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_otherSpending);
        }

        // GET: otherSpending/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_otherSpending tbl_otherSpending = db.tbl_otherSpending.Find(id);
            if (tbl_otherSpending == null)
            {
                return HttpNotFound();
            }
            return View(tbl_otherSpending);
        }

        // POST: otherSpending/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_otherSpending tbl_otherSpending = db.tbl_otherSpending.Find(id);
            db.tbl_otherSpending.Remove(tbl_otherSpending);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
