﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class HistoryOwnerDebtController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: HistoryOwnerDebt
        public ActionResult Index(int? id)
        {
            var tbl_HistoryOwnerDebt = db.tbl_HistoryOwnerDebt.Include(t => t.tbl_Owner_debt).Where(item=>item.fk_id_OwnerDebt==id).OrderByDescending(item => item.pk_id_HistoryOwnerDebt);
            return View(tbl_HistoryOwnerDebt.ToList());
        }

        // GET: HistoryOwnerDebt/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_HistoryOwnerDebt tbl_HistoryOwnerDebt = db.tbl_HistoryOwnerDebt.Find(id);
            if (tbl_HistoryOwnerDebt == null)
            {
                return HttpNotFound();
            }
            return View(tbl_HistoryOwnerDebt);
        }

        // GET: HistoryOwnerDebt/Create
        public ActionResult Create()
        {
            ViewBag.fk_id_OwnerDebt = new SelectList(db.tbl_Owner_debt, "pk_id_oDebt", "debt_status");
            return View();
        }

        // POST: HistoryOwnerDebt/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_HistoryOwnerDebt,fk_id_OwnerDebt,payment,date")] tbl_HistoryOwnerDebt tbl_HistoryOwnerDebt)
        {
            if (ModelState.IsValid)
            {
                db.tbl_HistoryOwnerDebt.Add(tbl_HistoryOwnerDebt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fk_id_OwnerDebt = new SelectList(db.tbl_Owner_debt, "pk_id_oDebt", "debt_status", tbl_HistoryOwnerDebt.fk_id_OwnerDebt);
            return View(tbl_HistoryOwnerDebt);
        }

        // GET: HistoryOwnerDebt/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_HistoryOwnerDebt tbl_HistoryOwnerDebt = db.tbl_HistoryOwnerDebt.Find(id);
            if (tbl_HistoryOwnerDebt == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_OwnerDebt = new SelectList(db.tbl_Owner_debt, "pk_id_oDebt", "debt_status", tbl_HistoryOwnerDebt.fk_id_OwnerDebt);
            return View(tbl_HistoryOwnerDebt);
        }

        // POST: HistoryOwnerDebt/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_HistoryOwnerDebt,fk_id_OwnerDebt,payment,date")] tbl_HistoryOwnerDebt tbl_HistoryOwnerDebt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_HistoryOwnerDebt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fk_id_OwnerDebt = new SelectList(db.tbl_Owner_debt, "pk_id_oDebt", "debt_status", tbl_HistoryOwnerDebt.fk_id_OwnerDebt);
            return View(tbl_HistoryOwnerDebt);
        }

        // GET: HistoryOwnerDebt/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_HistoryOwnerDebt tbl_HistoryOwnerDebt = db.tbl_HistoryOwnerDebt.Find(id);
            if (tbl_HistoryOwnerDebt == null)
            {
                return HttpNotFound();
            }
            return View(tbl_HistoryOwnerDebt);
        }

        // POST: HistoryOwnerDebt/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_HistoryOwnerDebt tbl_HistoryOwnerDebt = db.tbl_HistoryOwnerDebt.Find(id);
            db.tbl_HistoryOwnerDebt.Remove(tbl_HistoryOwnerDebt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
