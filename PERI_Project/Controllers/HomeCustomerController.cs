﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;
using System.IO;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace PERI_Project.Controllers
{
    public class HomeCustomerController : Controller
    {
        PERIEntityDB db = new PERIEntityDB();
        // GET: HomeCustomer
        public ActionResult HomePage()
        {
            Session["id_email"] = null;
            Session["id_Debt"] = null;
            var Banner = db.tbl_banner.ToList();
            var Product = db.tbl_product.ToList();

            ViewModelHome ViewHome = new ViewModelHome();
            ViewHome.banner = Banner;
            ViewHome.product = Product;
            ViewBag.Message = TempData["message"];
            ViewBag.Upload = TempData["msg"];
            return View(ViewHome);
        }

            public ActionResult About()
        {
            return View(db.tbl_about.ToList());
        }
        public ActionResult Details_Product(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_product tbl_product = db.tbl_product.Find(id);
            if (tbl_product == null)
            {
                return HttpNotFound();
            }
            return View(tbl_product);
        }

        public ActionResult Product()
        {
            return View(db.tbl_product.ToList());
        }

        
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_product tbl_product = db.tbl_product.Find(id);
            if (tbl_product == null)
            {
                return HttpNotFound();
            }
            return View(tbl_product);
        }

        public ActionResult OrderNow(int? id) {
                        tbl_Customer customer = Session["CustID"] as tbl_Customer;
            tbl_customerOrder customerOrder = new tbl_customerOrder();
            customerOrder.name = customer.Name;
            customerOrder.phone = customer.Phone;
            customerOrder.email = customer.Email;
            Session["CustomerOrder"] = customerOrder;

            return RedirectToAction("Order");
        }

        public ActionResult Order(int? id)
        {

            tbl_customerOrder custOrder = Session["CustomerOrder"] as tbl_customerOrder;
            //int idCust = (int)Session["CustID"];

            //int idOrder = (int)Session["CustIDOrder"];

            //tbl_customerOrder custOrder = db.tbl_customerOrder.Where(x => x.pk_id_customerOrder == idOrder && x.fk_id_customer == idCust).FirstOrDefault();
            tbl_cart order = new tbl_cart();
            order.fk_customer_id = custOrder.pk_id_customerOrder;
            order.fk_product_id = id;
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty");
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name");
            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Order(/*[Bind(Include = "pk_id_cart,fk_customer_id,details_order,lenght_meter,width_meter,fk_design_id,fk_product_id,amount,date")]*/ tbl_cart order_table)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //int idOrder = (int)Session["CustIDOrder"];
                        tbl_customerOrder custOrder = Session["CustomerOrder"] as tbl_customerOrder;
                        order_table.order_status = "Not Yet";
                        db.tbl_cart.Add(order_table);
                        order_table.date = DateTime.Now;
                        tbl_design design = db.tbl_design.Find(order_table.fk_design_id);
                        tbl_product product = db.tbl_product.Find(order_table.fk_product_id);
                        order_table.price = (order_table.lenght_meter * order_table.width_meter * product.product_price + design.design_price) * order_table.amount;


                        //#region // add upload design
                        //string filename = Path.GetFileNameWithoutExtension(order_table.ImageFile.FileName);
                        //string extension = Path.GetExtension(order_table.ImageFile.FileName);
                        //filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                        //order_table.ImagePath = "~/Content/images/order_design/" + filename;
                        //filename = Path.Combine(Server.MapPath("~/Content/images/order_design/"), filename);
                        //order_table.ImageFile.SaveAs(filename);
                        //#endregion

                        db.tbl_cart.Add(order_table);
                        db.SaveChanges();
                        ViewBag.TotalPrice = db.tbl_cart.Where(item => item.fk_customer_id == custOrder.pk_id_customerOrder).Select(item => item.price).DefaultIfEmpty().Sum();
                        //tbl_payment total = new tbl_payment();
                        //total.fk_id_customerOrder = order_table.fk_customer_id;
                        //double? totalprice = db.tbl_order.Where(item => item.fk_customer_id == total.fk_id_customerOrder).Select(item => item.price).DefaultIfEmpty().Sum();
                        //total.order_status = "Not Yet";
                        //total.total_price = totalprice;
                        //total.payment_status = "Not Yet";
                        //db.tbl_payment.Add(total);
                        //db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Product","HomeCustomer");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message;
                    }
                }
            }
            ViewBag.fk_customer_id = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", order_table.fk_customer_id);
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty", order_table.fk_design_id);
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name", order_table.fk_product_id);
            return View(order_table);
        }

        public ActionResult Payment()
        {
            tbl_customerOrder custOrder = Session["CustomerOrder"] as tbl_customerOrder;
            //int idOrder = (int)Session["CustIDOrder"];
            var cart = db.tbl_cart.Where(x => x.fk_customer_id == custOrder.pk_id_customerOrder);
            foreach (var item in cart) {
                tbl_order order = new tbl_order();
                order.fk_customer_id = item.fk_customer_id;
                order.details_order = item.details_order;
                order.lenght_meter = item.lenght_meter;
                order.width_meter = item.width_meter;
                order.fk_design_id = item.fk_design_id;
                order.fk_product_id = item.fk_product_id;
                order.amount = item.amount;
                order.price = item.price;
                order.date = item.date;
                order.order_status = item.order_status;
                order.ImagePath = item.ImagePath;
                db.tbl_order.Add(order);
                db.tbl_cart.Remove(item);
            }

            tbl_payment total = new tbl_payment();
            total.fk_id_customerOrder = custOrder.pk_id_customerOrder;
            double? totalprice = db.tbl_order.Where(item => item.fk_customer_id == total.fk_id_customerOrder).Select(item => item.price).DefaultIfEmpty().Sum();
            total.order_status = "Not Yet";
            total.total_price = totalprice;
            total.payment_status = "Not Yet";
            db.tbl_payment.Add(total);
            db.SaveChanges();
            // transaction.Commit();
            return RedirectToAction("HomePage", "HomeCustomer");
            //tbl_customerOrder customerOrder = new tbl_customerOrder();
            //var tbl_payment = db.tbl_payment.Where(x => x.fk_id_customerOrder == idOrder);
            //return View(tbl_payment.ToList());
        }
        
        public ActionResult Payment_Proof(int id)
        {
            tbl_payment payment = db.tbl_payment.Where(x => x.pk_id_payment == id).FirstOrDefault();
            return View(payment);
        }

        public ActionResult RequestOrder()
        {
            var product = db.tbl_product.Select(item => item.product_name).ToList();
            ViewData["Product"] = product;
            //ViewBag.fk_product_id = new SelectList(db.tbl_product, "product_name", "product_name");
            return View();
        }

        public ActionResult GetProductData()
        {
            var product = from als in db.tbl_product select new { als.product_name };
            return Json(product.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAdditionData()
        {
            var addition = from als in db.tbl_Addition select new { als.name };
            return Json(addition.ToList(), JsonRequestBehavior.AllowGet);
        }

        // POST: OrderRequest/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RequestOrder(tbl_OrderRequest tbl_OrderRequest)
        {
            tbl_OrderRequest.date = DateTime.Now;
            if (ModelState.IsValid)
            {
                tbl_OrderRequest.status = "Not Yet";
                db.tbl_OrderRequest.Add(tbl_OrderRequest);
                db.SaveChanges();
                string message = "Order has been Requested";
                TempData["message"] = message;
                return RedirectToAction("HomePage","HomeCustomer");
            }

            return View(tbl_OrderRequest);
        }

        public ActionResult Contact()
        {
            return View(db.tbl_contact.ToList());
        }

        public ActionResult CheckOrder()
        {
            ViewBag.Error = TempData["Error"];
            return View();
        }

        [HttpPost]
        public ActionResult CheckOrder(tbl_payment tbl_Payment)
        {

            var isNull = isPaymentNUll(tbl_Payment.pk_id_payment);
            if (isNull)
            {
                TempData["Error"] = "Your Payment ID doesn't Exist, Please Check again!";
                return RedirectToAction("CheckOrder", "HomeCustomer");
            }
            var payment = db.tbl_payment.Find(tbl_Payment.pk_id_payment);
            TempData["Check"] = payment;
            return RedirectToAction("CheckedOrder");
        }

        public ActionResult CheckedOrder()
        {
            var payment = TempData["Check"] as tbl_payment;
            if (payment == null)
            {
                return RedirectToAction("CheckOrder", "HomeCustomer");
            }
            tbl_payment paymentcheck = db.tbl_payment.Find(payment.pk_id_payment);
            return View(paymentcheck);
        }

        public ActionResult Upload(int id = 0)
        {
            tbl_UploadPayment Up = new tbl_UploadPayment();
            if (Session["id_email"] != null)
            {
                int Email = Convert.ToInt16(Session["id_email"]);
                Up.payment_id = Email;
            }
            
            ViewBag.Error = TempData["Error"];
            
            
            return View(Up);
        }

        public ActionResult UploadDebt(int id = 0)
        {

            ViewBag.Error = TempData["Error"];
            tbl_UploadDebt Up = new tbl_UploadDebt();
            if (Session["id_Debt"] != null)
            {
                int debt = Convert.ToInt16(Session["id_Debt"]);
                Up.Debt_id = debt;
            }

            return View(Up);
        }

        [NonAction]
        public bool isPaymentNUll(int? payment_id)
        {
            using (PERIEntityDB dc = new PERIEntityDB())
            {
                tbl_payment payment = db.tbl_payment.Where(item => item.pk_id_payment == payment_id).FirstOrDefault();
                return payment == null;
            }
        }

        public ActionResult getEmail(int id )
        {
            Session["id_email"] = id;
            return RedirectToAction("Upload");
        }

        public ActionResult getDebtID(int id)
        {
            Session["id_Debt"] = id;
            return RedirectToAction("UploadDebt");
        }

        public ActionResult OrderGuide()
        {
            return View();
        }


    }
}