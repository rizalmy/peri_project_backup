﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers
{
    public class HomeController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();
        public ActionResult Index()
        {
            int year = DateTime.Now.Year;
            ViewBag.Title = "Home Page";

            
            var spending = db.spGetSpendingTotal(year);
            double? a = spending.SingleOrDefault();//Select(item => item.Value).FirstOrDefault();
            ViewBag.SpendingTotal = a;

            var income = db.spGetIncomeTotal(year);
            double? b = income.SingleOrDefault();//Select(item => item.Value).FirstOrDefault();
            ViewBag.IncomeTotal = b;

            var profit = db.spGetProfitTotal(year);
            double? c = profit.SingleOrDefault();//Select(item => item.Value).FirstOrDefault();
            ViewBag.ProfitTotal = c;

            var AnnualProfit2 = db.spGetProfitAnnual2(year).ToList();
            var AnnualIncome2 = db.spGetIncomeAnnual2(year).ToList();
            var AnnualSpending2 = db.spGetSpendingAnnual2(year).ToList();

            ViewModelHome ViewHome = new ViewModelHome();
            ViewHome.annualIncome2 = AnnualIncome2;
            ViewHome.annualProfit2 = AnnualProfit2;
            ViewHome.annualSpending2 = AnnualSpending2;

            return View(ViewHome);
        }
    }
}
