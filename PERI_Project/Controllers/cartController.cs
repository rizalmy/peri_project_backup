﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers
{
    public class cartController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: cart
        public ActionResult Index()
        {
            int idOrder = (int)Session["CustIDOrder"];
            var tbl_cart = db.tbl_cart.Where(x => x.fk_customer_id == idOrder).Include(t => t.tbl_customerOrder).Include(t => t.tbl_design).Include(t => t.tbl_product);
           // var tbl_payment = db.tbl_payment.Where(x => x.fk_id_customerOrder == idOrder);
            return View(tbl_cart.ToList());
        }

        // GET: cart/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_cart tbl_cart = db.tbl_cart.Find(id);
            if (tbl_cart == null)
            {
                return HttpNotFound();
            }
            return View(tbl_cart);
        }

        // GET: cart/Create
        public ActionResult Create()
        {
            ViewBag.fk_customer_id = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name");
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty");
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name");
            return View();
        }

        // POST: cart/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_cart,fk_customer_id,details_order,lenght_meter,width_meter,fk_design_id,fk_product_id,amount,price,date,order_status,ImagePath")] tbl_cart tbl_cart)
        {
            if (ModelState.IsValid)
            {
                db.tbl_cart.Add(tbl_cart);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fk_customer_id = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", tbl_cart.fk_customer_id);
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty", tbl_cart.fk_design_id);
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name", tbl_cart.fk_product_id);
            return View(tbl_cart);
        }

        // GET: cart/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_cart tbl_cart = db.tbl_cart.Find(id);
            if (tbl_cart == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_customer_id = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", tbl_cart.fk_customer_id);
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty", tbl_cart.fk_design_id);
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name", tbl_cart.fk_product_id);
            return View(tbl_cart);
        }

        // POST: cart/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_cart,fk_customer_id,details_order,lenght_meter,width_meter,fk_design_id,fk_product_id,amount,price,date,order_status,ImagePath")] tbl_cart tbl_cart)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_cart).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fk_customer_id = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", tbl_cart.fk_customer_id);
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty", tbl_cart.fk_design_id);
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name", tbl_cart.fk_product_id);
            return View(tbl_cart);
        }

        // GET: cart/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_cart tbl_cart = db.tbl_cart.Find(id);
            if (tbl_cart == null)
            {
                return HttpNotFound();
            }
            return View(tbl_cart);
        }

        // POST: cart/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_cart tbl_cart = db.tbl_cart.Find(id);
            db.tbl_cart.Remove(tbl_cart);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
