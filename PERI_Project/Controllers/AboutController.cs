﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;
using System.IO;

namespace PERI_Project.Controllers
{
    public class AboutController : Controller
    {
        PERIEntityDB db = new PERIEntityDB();
        // GET: About
        public ActionResult Index()
        {
            return View(db.tbl_about.ToList());
        }

        // GET: product/Create

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_about about = db.tbl_about.Find(id);
            if (about == null)
            {
                return HttpNotFound();
            }
            return View(about);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var about = db.tbl_about.Find(id);
            Session["imgPath"] = about.Profile_Image;
            if (about == null)
            {
                return HttpNotFound();
            }
            return View(about);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_about about)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (about.ImageFile != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(about.ImageFile.FileName);
                        string extension = Path.GetExtension(about.ImageFile.FileName);
                        filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                        about.Profile_Image = "~/Content/images/about/" + filename;
                        filename = Path.Combine(Server.MapPath("~/Content/images/about/"), filename);

                        if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
                        {
                            if (about.ImageFile.ContentLength <= 500000)
                            {
                                db.Entry(about).State = EntityState.Modified;
                                string oldImgPath = Request.MapPath(Session["imgPath"].ToString());

                                if (db.SaveChanges() > 0)
                                {
                                    about.ImageFile.SaveAs(filename);

                                    if (System.IO.File.Exists(oldImgPath))
                                    {
                                        System.IO.File.Delete(oldImgPath);
                                    }
                                    TempData["msg"] = "Successful about data updated";
                                    return RedirectToAction("Index", "About");
                                }

                            }
                            else
                            {
                                ViewBag.msg = "File Size must be Equal or less than 500kb";
                            }
                        }
                        else
                        {
                            ViewBag.msg = "Invalid File Type ! Select file type [ jpg or png ]";
                        }
                    }
                    else
                    {
                        about.Profile_Image = Session["imgPath"].ToString();
                        db.Entry(about).State = EntityState.Modified;
                        if (db.SaveChanges() > 0)
                        {
                            TempData["msg"] = "Successful about data updated";
                            return RedirectToAction("Index", "About");
                        }
                    }
                }
                catch (Exception error)
                {
                    throw error;
                }
            }
            return View();
        }

    }
}