﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Timers;
using System.Net;
using System.Net.Mail;

namespace LVL2_ASPNet_MVC_49
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer tmrExecutor = new System.Timers.Timer();
        int ScheduleTime = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadTime"]);
        public Thread worker = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                tmrExecutor.Elapsed += new ElapsedEventHandler(tmrExecutor_Elapsed); //add event
                tmrExecutor.Interval = 5000; // set time
                tmrExecutor.Enabled = true;
                tmrExecutor.Start();
                /*
                ThreadStart start = new ThreadStart(working);
                worker = new Thread(start);
                worker.Start();
                */
            }
            catch (Exception)
            {
                throw;
            }

        }
        private void tmrExecutor_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //work here
                Peri_DBEntities db = new Peri_DBEntities();
                var ownerdebt = db.tbl_Owner_debt.Where(item => item.debt_status == "Not Yet");
                var customerdebt = db.tbl_customerDebt.Where(item => item.debt_status == "Not Yet");
                tbl_contact contact = db.tbl_contact.Find(1);
                string ContactData = contact.email;
                string data = "";
                if (ownerdebt != null || customerdebt != null)
                {
                    if (ownerdebt != null)
                    {
                        data += "Hutang Owner<br/>";
                        foreach (var item in ownerdebt)
                        {
                            data +=
                                "-----------------------------------------------<br/>Name : " +
                                item.tbl_spending.tbl_spendingItem.name +
                                "<br/>Debt : " + item.debt + " " +
                                "<br/>Date : " + item.date +
                                "<br/><br/>------------------------------------------<br/><br/>";
                        }
                    }

                    if (customerdebt != null)
                    {
                        data += "Hutang Customer<br/>";
                        foreach (var item in customerdebt)
                        {
                            data +=
                                "-----------------------------------------------<br/>Name : " +
                                item.tbl_payment.tbl_customerOrder.name +
                                "<br/>Debt : " + item.Debt + " " +
                                "<br/>Date : " + item.date +
                                "<br/><br/>------------------------------------------<br/><br/>";
                        }
                    }

                    var fromEmail = new MailAddress("periofficial.company@gmail.com", "PERI");
                    var toEmail = new MailAddress(ContactData);
                    var fromEmailPassword = "logPeri987";
                    string subject = "Debt";
                    string body = data;

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = true,
                        Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
                    };

                    using (var msg = new MailMessage(fromEmail, toEmail)
                    {
                        Subject = subject,
                        Body = body,
                        IsBodyHtml = true

                    })
                        smtp.Send(msg);
                }


                


                Thread.Sleep(ScheduleTime + 60 + 1000);
            }
            catch (Exception)
            {
                throw;
            }
           
        }
        public void working()
        {
            while(true)
            {
                string path = "C:\\sample.txt";
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(string.Format("Windows Service is called on" + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ""));
                    writer.Close();
                }
                Thread.Sleep(ScheduleTime + 60 + 1000);
            }
        }

        protected override void OnStop()
        {
            try
            {
                tmrExecutor.Enabled = false;
                /*
                if ((worker != null) & worker.IsAlive)
                {
                    worker.Abort();
                }
                */
            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}
