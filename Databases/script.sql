USE [master]
GO
/****** Object:  Database [Peri_DB]    Script Date: 6/14/2020 8:33:24 PM ******/
CREATE DATABASE [Peri_DB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Peri_DB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Peri_DB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Peri_DB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Peri_DB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Peri_DB] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Peri_DB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Peri_DB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Peri_DB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Peri_DB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Peri_DB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Peri_DB] SET ARITHABORT OFF 
GO
ALTER DATABASE [Peri_DB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Peri_DB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Peri_DB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Peri_DB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Peri_DB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Peri_DB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Peri_DB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Peri_DB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Peri_DB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Peri_DB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Peri_DB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Peri_DB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Peri_DB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Peri_DB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Peri_DB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Peri_DB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Peri_DB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Peri_DB] SET RECOVERY FULL 
GO
ALTER DATABASE [Peri_DB] SET  MULTI_USER 
GO
ALTER DATABASE [Peri_DB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Peri_DB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Peri_DB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Peri_DB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Peri_DB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Peri_DB] SET QUERY_STORE = OFF
GO
USE [Peri_DB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Peri_DB]
GO
/****** Object:  Table [dbo].[tbl_about]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_about](
	[AboutID] [int] IDENTITY(1,1) NOT NULL,
	[Profile_Image] [varchar](max) NULL,
	[Main_Title] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[AboutID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Addition]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Addition](
	[pk_id_addition] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[price] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_addition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Admin]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Admin](
	[AdminID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[fk_RoleID] [int] NULL,
 CONSTRAINT [PK_tbl_Admin] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_banner]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_banner](
	[BannerID] [int] IDENTITY(1,1) NOT NULL,
	[Main_Title] [varchar](max) NULL,
	[Second_Title] [varchar](max) NULL,
	[Banner_Image] [varchar](max) NULL,
 CONSTRAINT [PK_tbl_banner] PRIMARY KEY CLUSTERED 
(
	[BannerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_cart]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_cart](
	[pk_id_cart] [int] IDENTITY(1,1) NOT NULL,
	[fk_customer_id] [int] NULL,
	[details_order] [varchar](600) NULL,
	[lenght_meter] [float] NULL,
	[width_meter] [float] NULL,
	[fk_design_id] [int] NULL,
	[fk_product_id] [int] NULL,
	[amount] [float] NULL,
	[price] [float] NULL,
	[date] [date] NULL,
	[order_status] [varchar](20) NULL,
	[ImagePath] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_cart] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_contact]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_contact](
	[pk_id_contact] [int] IDENTITY(1,1) NOT NULL,
	[address] [varchar](300) NULL,
	[phone] [varchar](20) NULL,
	[email] [varchar](50) NULL,
	[Image] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_contact] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Customer]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Customer](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Password] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[Name] [varchar](100) NULL,
	[Phone] [varchar](15) NULL,
	[NoKtp] [varchar](16) NULL,
	[Address] [varchar](300) NULL,
	[ActivationCode] [uniqueidentifier] NULL,
	[ImagePath] [varchar](max) NULL,
	[ResetPasswordCode] [varchar](100) NULL,
 CONSTRAINT [PK_tbl_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_customerDebt]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_customerDebt](
	[pk_id_cDebt] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_payment] [int] NULL,
	[Debt] [float] NULL,
	[debt_status] [varchar](20) NULL,
	[date] [datetime] NULL,
	[payment] [float] NULL,
	[ImagePath] [varchar](max) NULL,
	[BankName] [varchar](50) NULL,
	[BankAccountName] [varchar](50) NULL,
	[BankAccountNumber] [varchar](30) NULL,
 CONSTRAINT [PK_tbl_customerDebt] PRIMARY KEY CLUSTERED 
(
	[pk_id_cDebt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_customerOrder]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_customerOrder](
	[pk_id_customerOrder] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_customer] [int] NULL,
	[name] [varchar](50) NULL,
	[phone] [varchar](50) NULL,
	[email] [varchar](50) NULL,
 CONSTRAINT [PK_tbl_customerOrder] PRIMARY KEY CLUSTERED 
(
	[pk_id_customerOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_depositPayment]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_depositPayment](
	[pk_id_depositPayment] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_payment] [int] NULL,
	[deposit] [float] NULL,
	[date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_depositPayment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_depositSpending]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_depositSpending](
	[pk_id_depositSpending] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_spending] [int] NULL,
	[deposit] [float] NULL,
	[date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_depositSpending] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_design]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_design](
	[pk_id_design] [int] IDENTITY(1,1) NOT NULL,
	[difficulty] [varchar](50) NULL,
	[design_price] [float] NULL,
 CONSTRAINT [PK_tbl_design] PRIMARY KEY CLUSTERED 
(
	[pk_id_design] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HistoryCustomerDebt]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HistoryCustomerDebt](
	[pk_id_HistoryCustomerDebt] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_CustomerDebt] [int] NULL,
	[payment] [float] NULL,
	[date] [datetime] NULL,
	[ImagePath] [varchar](max) NULL,
	[BankName] [varchar](50) NULL,
	[BankAccountName] [varchar](50) NULL,
	[BankAccountNumber] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_HistoryCustomerDebt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HistoryOwnerDebt]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HistoryOwnerDebt](
	[pk_id_HistoryOwnerDebt] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_OwnerDebt] [int] NULL,
	[payment] [float] NULL,
	[date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_HistoryOwnerDebt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_order]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_order](
	[pk_id_order] [int] IDENTITY(1,1) NOT NULL,
	[fk_customer_id] [int] NULL,
	[details_order] [varchar](600) NULL,
	[lenght_meter] [float] NULL,
	[width_meter] [float] NULL,
	[fk_design_id] [int] NULL,
	[fk_product_id] [int] NULL,
	[amount] [float] NULL,
	[price] [float] NULL,
	[date] [datetime] NULL,
	[order_status] [varchar](20) NULL,
	[ImagePath] [varchar](max) NULL,
	[fk_id_addition] [int] NULL,
 CONSTRAINT [PK_tbl_order] PRIMARY KEY CLUSTERED 
(
	[pk_id_order] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_OrderRequest]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_OrderRequest](
	[pk_id_OrderRequest] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[address] [varchar](250) NULL,
	[phone] [varchar](20) NULL,
	[email] [varchar](50) NULL,
	[description] [varchar](max) NULL,
	[status] [varchar](20) NULL,
	[date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_OrderRequest] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_otherIncome]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_otherIncome](
	[pk_id_otherIncome] [int] IDENTITY(1,1) NOT NULL,
	[incomeName] [varchar](50) NULL,
	[income] [float] NULL,
	[date] [datetime] NULL,
 CONSTRAINT [PK_tbl_otherIncome] PRIMARY KEY CLUSTERED 
(
	[pk_id_otherIncome] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_otherSpending]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_otherSpending](
	[pk_id_otherSpending] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[spending_value] [float] NULL,
	[date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_otherSpending] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Owner_debt]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Owner_debt](
	[pk_id_oDebt] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_spending] [int] NULL,
	[debt_status] [varchar](20) NULL,
	[date] [datetime] NULL,
	[payment] [float] NULL,
	[debt] [float] NULL,
 CONSTRAINT [PK_tbl_Owner_debt] PRIMARY KEY CLUSTERED 
(
	[pk_id_oDebt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_payment]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_payment](
	[pk_id_payment] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_customerOrder] [int] NULL,
	[total_price] [float] NULL,
	[payment] [float] NULL,
	[payment_status] [varchar](20) NULL,
	[order_status] [varchar](20) NULL,
	[date] [datetime] NULL,
	[ImagePath] [varchar](max) NULL,
	[estimasi] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[BankAccountName] [varchar](50) NULL,
	[BankAccountNumber] [varchar](30) NULL,
 CONSTRAINT [PK_tbl_payment] PRIMARY KEY CLUSTERED 
(
	[pk_id_payment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_product]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_product](
	[pk_id_product] [int] IDENTITY(1,1) NOT NULL,
	[product_name] [varchar](50) NULL,
	[product_price] [float] NULL,
	[ImagePath] [varchar](max) NULL,
	[description] [varchar](max) NULL,
 CONSTRAINT [PK_tbl_product] PRIMARY KEY CLUSTERED 
(
	[pk_id_product] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RoleUser]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RoleUser](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[Role_User] [varchar](20) NULL,
 CONSTRAINT [PK_tbl_RoleUser] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_spending]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_spending](
	[pk_id_spending] [int] IDENTITY(1,1) NOT NULL,
	[payment] [float] NULL,
	[date] [datetime] NULL,
	[fk_id_spendingItem] [int] NULL,
	[payment_status] [varchar](20) NULL,
 CONSTRAINT [PK_tbl_spending] PRIMARY KEY CLUSTERED 
(
	[pk_id_spending] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_spendingItem]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_spendingItem](
	[pk_id_spendingItem] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[price] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_spendingItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_UploadDebt]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_UploadDebt](
	[pk_id_UploadDebt] [int] IDENTITY(1,1) NOT NULL,
	[Debt_id] [int] NULL,
	[ImagePath] [varchar](max) NULL,
	[BankName] [varchar](50) NULL,
	[BankAccountName] [varchar](50) NULL,
	[BankAccountNumber] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_UploadDebt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_UploadPayment]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_UploadPayment](
	[pk_id_UploadPayment] [int] IDENTITY(1,1) NOT NULL,
	[payment_id] [int] NULL,
	[ImagePath] [varchar](max) NULL,
	[BankName] [varchar](50) NULL,
	[BankAccountName] [varchar](50) NULL,
	[BankAccountNumber] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_UploadPayment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_FK__tbl_Admin__fk_Ro__3B75D760]    Script Date: 6/14/2020 8:33:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK__tbl_Admin__fk_Ro__3B75D760] ON [dbo].[tbl_Admin]
(
	[fk_RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_fkpaymentdebt]    Script Date: 6/14/2020 8:33:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_fkpaymentdebt] ON [dbo].[tbl_customerDebt]
(
	[fk_id_payment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_fk_customer]    Script Date: 6/14/2020 8:33:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_fk_customer] ON [dbo].[tbl_customerOrder]
(
	[fk_id_customer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_fk_customerOrder_order]    Script Date: 6/14/2020 8:33:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_fk_customerOrder_order] ON [dbo].[tbl_order]
(
	[fk_customer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_fk_designorder]    Script Date: 6/14/2020 8:33:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_fk_designorder] ON [dbo].[tbl_order]
(
	[fk_design_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_fk_productOrder]    Script Date: 6/14/2020 8:33:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_fk_productOrder] ON [dbo].[tbl_order]
(
	[fk_product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_fkOdebtSpending]    Script Date: 6/14/2020 8:33:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_fkOdebtSpending] ON [dbo].[tbl_Owner_debt]
(
	[fk_id_spending] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_fk_customerOrder_payment]    Script Date: 6/14/2020 8:33:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_fk_customerOrder_payment] ON [dbo].[tbl_payment]
(
	[fk_id_customerOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_Admin]  WITH CHECK ADD  CONSTRAINT [FK__tbl_Admin__fk_Ro__3B75D760] FOREIGN KEY([fk_RoleID])
REFERENCES [dbo].[tbl_RoleUser] ([RoleID])
GO
ALTER TABLE [dbo].[tbl_Admin] CHECK CONSTRAINT [FK__tbl_Admin__fk_Ro__3B75D760]
GO
ALTER TABLE [dbo].[tbl_cart]  WITH CHECK ADD  CONSTRAINT [fk_customerOrder_Cart] FOREIGN KEY([fk_customer_id])
REFERENCES [dbo].[tbl_customerOrder] ([pk_id_customerOrder])
GO
ALTER TABLE [dbo].[tbl_cart] CHECK CONSTRAINT [fk_customerOrder_Cart]
GO
ALTER TABLE [dbo].[tbl_cart]  WITH CHECK ADD  CONSTRAINT [fk_designCart] FOREIGN KEY([fk_design_id])
REFERENCES [dbo].[tbl_design] ([pk_id_design])
GO
ALTER TABLE [dbo].[tbl_cart] CHECK CONSTRAINT [fk_designCart]
GO
ALTER TABLE [dbo].[tbl_cart]  WITH CHECK ADD  CONSTRAINT [fk_productCart] FOREIGN KEY([fk_product_id])
REFERENCES [dbo].[tbl_product] ([pk_id_product])
GO
ALTER TABLE [dbo].[tbl_cart] CHECK CONSTRAINT [fk_productCart]
GO
ALTER TABLE [dbo].[tbl_customerDebt]  WITH CHECK ADD  CONSTRAINT [FK_fkpaymentdebt] FOREIGN KEY([fk_id_payment])
REFERENCES [dbo].[tbl_payment] ([pk_id_payment])
GO
ALTER TABLE [dbo].[tbl_customerDebt] CHECK CONSTRAINT [FK_fkpaymentdebt]
GO
ALTER TABLE [dbo].[tbl_customerOrder]  WITH CHECK ADD  CONSTRAINT [fk_customer] FOREIGN KEY([fk_id_customer])
REFERENCES [dbo].[tbl_Customer] ([CustomerID])
GO
ALTER TABLE [dbo].[tbl_customerOrder] CHECK CONSTRAINT [fk_customer]
GO
ALTER TABLE [dbo].[tbl_depositPayment]  WITH CHECK ADD  CONSTRAINT [fk_depositPayment] FOREIGN KEY([fk_id_payment])
REFERENCES [dbo].[tbl_payment] ([pk_id_payment])
GO
ALTER TABLE [dbo].[tbl_depositPayment] CHECK CONSTRAINT [fk_depositPayment]
GO
ALTER TABLE [dbo].[tbl_depositSpending]  WITH CHECK ADD  CONSTRAINT [fk_depositSpending] FOREIGN KEY([fk_id_spending])
REFERENCES [dbo].[tbl_spending] ([pk_id_spending])
GO
ALTER TABLE [dbo].[tbl_depositSpending] CHECK CONSTRAINT [fk_depositSpending]
GO
ALTER TABLE [dbo].[tbl_HistoryCustomerDebt]  WITH CHECK ADD  CONSTRAINT [fk_CustomerDebt] FOREIGN KEY([fk_id_CustomerDebt])
REFERENCES [dbo].[tbl_customerDebt] ([pk_id_cDebt])
GO
ALTER TABLE [dbo].[tbl_HistoryCustomerDebt] CHECK CONSTRAINT [fk_CustomerDebt]
GO
ALTER TABLE [dbo].[tbl_HistoryOwnerDebt]  WITH CHECK ADD  CONSTRAINT [fk_OwnerDebt] FOREIGN KEY([fk_id_OwnerDebt])
REFERENCES [dbo].[tbl_Owner_debt] ([pk_id_oDebt])
GO
ALTER TABLE [dbo].[tbl_HistoryOwnerDebt] CHECK CONSTRAINT [fk_OwnerDebt]
GO
ALTER TABLE [dbo].[tbl_order]  WITH CHECK ADD  CONSTRAINT [fk_customerOrder_order] FOREIGN KEY([fk_customer_id])
REFERENCES [dbo].[tbl_customerOrder] ([pk_id_customerOrder])
GO
ALTER TABLE [dbo].[tbl_order] CHECK CONSTRAINT [fk_customerOrder_order]
GO
ALTER TABLE [dbo].[tbl_order]  WITH CHECK ADD  CONSTRAINT [fk_designorder] FOREIGN KEY([fk_design_id])
REFERENCES [dbo].[tbl_design] ([pk_id_design])
GO
ALTER TABLE [dbo].[tbl_order] CHECK CONSTRAINT [fk_designorder]
GO
ALTER TABLE [dbo].[tbl_order]  WITH CHECK ADD  CONSTRAINT [fk_id_addition] FOREIGN KEY([fk_id_addition])
REFERENCES [dbo].[tbl_Addition] ([pk_id_addition])
GO
ALTER TABLE [dbo].[tbl_order] CHECK CONSTRAINT [fk_id_addition]
GO
ALTER TABLE [dbo].[tbl_order]  WITH CHECK ADD  CONSTRAINT [fk_productOrder] FOREIGN KEY([fk_product_id])
REFERENCES [dbo].[tbl_product] ([pk_id_product])
GO
ALTER TABLE [dbo].[tbl_order] CHECK CONSTRAINT [fk_productOrder]
GO
ALTER TABLE [dbo].[tbl_Owner_debt]  WITH CHECK ADD  CONSTRAINT [FK_fkOdebtSpending] FOREIGN KEY([fk_id_spending])
REFERENCES [dbo].[tbl_spending] ([pk_id_spending])
GO
ALTER TABLE [dbo].[tbl_Owner_debt] CHECK CONSTRAINT [FK_fkOdebtSpending]
GO
ALTER TABLE [dbo].[tbl_payment]  WITH CHECK ADD  CONSTRAINT [fk_customerOrder_payment] FOREIGN KEY([fk_id_customerOrder])
REFERENCES [dbo].[tbl_customerOrder] ([pk_id_customerOrder])
GO
ALTER TABLE [dbo].[tbl_payment] CHECK CONSTRAINT [fk_customerOrder_payment]
GO
ALTER TABLE [dbo].[tbl_spending]  WITH CHECK ADD FOREIGN KEY([fk_id_spendingItem])
REFERENCES [dbo].[tbl_spendingItem] ([pk_id_spendingItem])
GO
/****** Object:  StoredProcedure [dbo].[spGetIncomeAnnual]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetIncomeAnnual](@paramYear as int)
AS
BEGIN
select Month(tbl_payment.date) as Month,coalesce(sum(coalesce(tbl_payment.payment,0)+coalesce(tbl_otherIncome.income,0)-coalesce(tbl_otherIncome.income,0)),0) as Income 
from tbl_payment left join tbl_otherIncome on tbl_payment.date=tbl_otherIncome.date
where year(tbl_payment.date)=@paramYear
group by MONTH(tbl_payment.date)
END
GO
/****** Object:  StoredProcedure [dbo].[spGetIncomeAnnual2]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetIncomeAnnual2](@paramYear as int)
AS
BEGIN
select DateName( month , DateAdd( month , Tpayment.date , 0 ) - 1 ) as Month, coalesce(coalesce(Tpayment.payment,0)+coalesce(Tincome.income,0),0) as Income
from
(select sum(payment)as payment, Month(date)as date from tbl_payment where year(date)=@paramYear group by Month(date))as Tpayment 
left join (select sum(income) as income, Month(date)as date from tbl_otherIncome where year(date)=@paramYear group by Month(date)) as Tincome 
on Tpayment.date=Tincome.date
END
GO
/****** Object:  StoredProcedure [dbo].[spGetIncomeMonth]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetIncomeMonth](@paramYear as int, @paramMonth as int)
AS
BEGIN
select tbl_payment.date,
coalesce(sum(coalesce(tbl_payment.payment,0)+coalesce(tbl_otherIncome.income,0)),0) as Income 
from tbl_payment left join tbl_otherIncome on tbl_payment.date=tbl_otherIncome.date
where year(tbl_payment.date)=@paramYear and month(tbl_payment.date)=@paramMonth
group by tbl_payment.date
END
GO
/****** Object:  StoredProcedure [dbo].[spGetIncomeTotal]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetIncomeTotal](@paramYear as int)
AS
BEGIN
select coalesce(coalesce(Tpayment.payment,0)+coalesce(Tincome.income,0),0) as income_year
from(select sum(payment)as payment, year(date)as date from tbl_payment where year(date)=@paramYear  group by year(date))as Tpayment 
left join (select sum(income) as income, year(date)as date from tbl_otherIncome where year(date)=@paramYear  group by year(date)) as Tincome on Tpayment.date=Tincome.date
END
GO
/****** Object:  StoredProcedure [dbo].[spGetProfitAnnual]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetProfitAnnual](@paramYear as int)
AS
BEGIN
select Month(tbl_payment.date) as Month,coalesce(sum(coalesce(tbl_payment.payment,0)+coalesce(tbl_otherIncome.income,0)-coalesce(tbl_spending.payment,0)),0) as Income 
from tbl_payment left join tbl_otherIncome on tbl_payment.date=tbl_otherIncome.date
left join tbl_spending on tbl_payment.date=tbl_spending.date
where year(tbl_payment.date)=@paramYear
group by MONTH(tbl_payment.date)
END
GO
/****** Object:  StoredProcedure [dbo].[spGetProfitAnnual2]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetProfitAnnual2](@paramYear as int)
AS
BEGIN
select  DateName( month , DateAdd( month , Income_total.date , 0 ) - 1 ) as Month, coalesce(Income_total.Total_Income,0)-coalesce(Spending_Total.total_spending,0) as Profit
from
(select coalesce(coalesce(Tpayment.payment,0)+coalesce(Tincome.income,0),0) as Total_Income, Tpayment.date as date
from
(select sum(payment)as payment, Month(date)as date from tbl_payment where year(date)=@paramYear group by Month(date))as Tpayment 
left join (select sum(income) as income, Month(date)as date from tbl_otherIncome where year(date)=@paramYear group by Month(date)) as Tincome 
on Tpayment.date=Tincome.date) as Income_total 
left join
(select coalesce(coalesce(t1.spending,0)+coalesce(t2.spending,0),0) as total_spending, t1.date as date
from
(select sum(payment) as spending, month(date)as date from tbl_spending where YEAR(date)=@paramYear group by month(date)
) as t1
left join
(select sum(spending_value) as spending , month(date) as date from tbl_otherSpending where year(date)=@paramYear group by month(date)) as t2
on t1.date=t2.date) as Spending_Total
on Income_total.date=Spending_Total.date order by Income_total.date
END
GO
/****** Object:  StoredProcedure [dbo].[spGetProfitMonth]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetProfitMonth](@paramYear as int, @paramMonth as int)
AS
BEGIN
select tbl_payment.date,
coalesce(sum(coalesce(tbl_payment.payment,0)+coalesce(tbl_otherIncome.income,0)-coalesce(tbl_spending.payment,0)),0) as Profit 
from tbl_payment left join tbl_otherIncome on tbl_payment.date=tbl_otherIncome.date
left join tbl_spending on tbl_payment.date=tbl_spending.date
where year(tbl_payment.date)=@paramYear and month(tbl_payment.date)=@paramMonth
group by tbl_payment.date
END
GO
/****** Object:  StoredProcedure [dbo].[spGetProfitTotal]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetProfitTotal](@paramYear as int)
AS
BEGIN
select coalesce(Income_total.Total_Income,0)-coalesce(Spending_Total.total_spending,0) as profit_year
from
(select coalesce(coalesce(Tpayment.payment,0)+coalesce(Tincome.income,0),0) as Total_Income, Tpayment.date as date
from(select sum(payment)as payment, year(date)as date from tbl_payment where year(date)=@paramYear  group by year(date))as Tpayment 
left join (select sum(income) as income, year(date)as date from tbl_otherIncome where year(date)=@paramYear  group by year(date)) as Tincome on Tpayment.date=Tincome.date) as Income_total 
left join
(select coalesce(coalesce(t1.spending,0)+coalesce(t2.spending,0),0) as total_spending, t1.date as date
from
(select sum(payment) as spending, year(date)as date from tbl_spending where YEAR(date)=@paramYear group by year(date)) as t1
left join
(select sum(spending_value) as spending , year(date) as date from tbl_otherSpending where year(date)=@paramYear group by year(date)) as t2
on t1.date=t2.date) as Spending_Total
on Income_total.date=Spending_Total.date
END
GO
/****** Object:  StoredProcedure [dbo].[spGetSpendingAnnual]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetSpendingAnnual](@paramYear as int)
AS
BEGIN
select Month(date) as Month,coalesce(sum(coalesce(tbl_spending.payment,0)),0) as spending
from tbl_spending
where year(date)=@paramYear
group by MONTH(date)
END
GO
/****** Object:  StoredProcedure [dbo].[spGetSpendingAnnual2]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetSpendingAnnual2](@paramYear as int)
AS
BEGIN
select  DateName( month , DateAdd( month , t1.date , 0 ) - 1 ) as Month, coalesce(coalesce(t1.spending,0)+coalesce(t2.spending,0),0) as spending
from
(select sum(payment) as spending, month(date)as date from tbl_spending where YEAR(date)=@paramYear group by month(date)
) as t1
left join
(select sum(spending_value) as spending , month(date) as date from tbl_otherSpending where year(date)=@paramYear group by month(date)) as t2
on t1.date=t2.date
END
GO
/****** Object:  StoredProcedure [dbo].[spGetSpendingMonth]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetSpendingMonth](@paramYear as int, @paramMonth as int)
AS
BEGIN
select date,coalesce(sum(coalesce(tbl_spending.payment,0)),0) as spending 
from tbl_spending
where year(date)=@paramYear and month(date)=@paramMonth
group by date
END
GO
/****** Object:  StoredProcedure [dbo].[spGetSpendingTotal]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetSpendingTotal](@paramYear as int)
AS
BEGIN
select coalesce(coalesce(t1.spending,0)+coalesce(t2.spending,0),0) as Spending_Year
from
(select sum(payment) as spending, year(date)as date from tbl_spending where YEAR(date)=@paramYear group by year(date)) as t1
left join
(select sum(spending_value) as spending , year(date) as date from tbl_otherSpending where year(date)=@paramYear group by year(date)) as t2
on t1.date=t2.date
END
GO
/****** Object:  StoredProcedure [dbo].[SpInsertDate]    Script Date: 6/14/2020 8:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInsertDate]
(   @total_price     float,  
    @payment         float,  
    @payment_status  varchar(20),  
    @order_status    VARCHAR(20),  
    @date            datetime) 
AS
BEGIN
            INSERT INTO tbl_payment 
                        (total_price,  
                         payment,  
                         payment_status,  
                         order_status,  
                         date)  
            VALUES     ( @total_price,  
                         @payment,  
                         @payment_status,  
                         @order_status,  
                         @date)  
END
GO
USE [master]
GO
ALTER DATABASE [Peri_DB] SET  READ_WRITE 
GO
