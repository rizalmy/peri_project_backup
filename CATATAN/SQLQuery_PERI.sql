----- 16/05/2020 ------
 --> add column tbl_customer
 alter table tbl_Customer
 add ResetPasswordCode varchar(100)

 --> add tbl_banner
 create table tbl_banner (
 BannerID int identity primary key,
 Main_Title varchar(max),
 Second_Title varchar(max),
 Banner_Image varchar(max)
 )

 
--> add tbl_about
 create table tbl_about (
 AboutID int identity primary key,
 Profile_Image varchar(max),
 Main_Title varchar(50),
 Description varchar(max)
 )

---- 26/05/2020 ----------
--> add column estimasi 
alter table tbl_payment
add estimasi varchar(50)

---- 28/05/2020 --------
--> add column image
alter table tbl_contact
add Image varchar(max)